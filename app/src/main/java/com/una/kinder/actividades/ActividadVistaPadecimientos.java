package com.una.kinder.actividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CalendarView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.adapters.AdapterEvents;
import com.una.kinder.adapters.AdapterNinos;
import com.una.kinder.adapters.AdapterNinosPadecimiento;
import com.una.kinder.entidades.Evento;
import com.una.kinder.entidades.Nino;
import com.una.kinder.entidades.Padecimiento;
import com.una.kinder.entidades.Usuario;

import java.util.ArrayList;
//MOSTRAR LA LISTA CON LOS NIÑOS PARA ADD PADECIMIENTO
public class ActividadVistaPadecimientos extends AppCompatActivity {
    private RecyclerView reciclador;
    ArrayList<Nino> listaNinos;
    AdapterNinosPadecimiento adapter;

    FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_vista_padecimientos);
        setTitle ("Padecimientos");
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
         reciclador = (RecyclerView) this.findViewById(R.id.reciclador);
       // reciclador.setAdapter(adapter);

        if(auth.getCurrentUser()!=null) {

            database.getReference(FirebaseUtils.CHILD_USUARIO).addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Usuario usuario = dataSnapshot.child(auth.getCurrentUser().getUid()).getValue(Usuario.class);
                    System.out.println("esto es una prueba2222222222222222222");
                    reciclador = (RecyclerView) findViewById(R.id.listaninop);
                    reciclador.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    listaNinos = new ArrayList<>();
                    adapter = new AdapterNinosPadecimiento(listaNinos);






                    if (usuario.getTipo().equals( FirebaseUtils.TIPO_USUARIO_PADRE)) {
                        System.out.println("esto es una pruebaaaaaaaaaaaaa");
                        myRef = database.getReference(FirebaseUtils.CHILD_PADRE).child(auth.getCurrentUser().getUid()).child("ninos");

                        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reciclador.getContext(), LinearLayoutManager.VERTICAL);
                        reciclador.addItemDecoration(dividerItemDecoration);
                        database.getReference(FirebaseUtils.CHILD_PADRE).child(auth.getCurrentUser().getUid()).child("ninos").addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                String nino = dataSnapshot.getKey();
                                database.getReference(FirebaseUtils.CHILD_NINO).child(nino).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        boolean flag = false;
                                        Nino nino = dataSnapshot.getValue(Nino.class);
                                        if (nino != null) {

                                            for (int i = 0; i < listaNinos.size(); i++){
                                                if (listaNinos.get(i).getLlave() == nino.getLlave()) {
                                                    listaNinos.set(i, nino);
                                                    adapter.notifyDataSetChanged();
                                                    i = 100;
                                                    flag = true;
                                                }}
                                            if (flag == false) {
                                                listaNinos.add(nino);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                        reciclador.setAdapter(adapter);
                    }
                    else{
                        myRef = database.getReference(FirebaseUtils.CHILD_NINO);


                        myRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                listaNinos.clear();
                                for (DataSnapshot item : dataSnapshot.getChildren()) {
                                    Nino nino = item.getValue(Nino.class);
                                    listaNinos.add(nino);
                                }
                                adapter = new AdapterNinosPadecimiento(listaNinos);
                                reciclador.setAdapter(adapter);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        reciclador.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        adapter = new AdapterNinosPadecimiento(listaNinos);
                        reciclador.setAdapter(adapter);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });



        }
    }


}
