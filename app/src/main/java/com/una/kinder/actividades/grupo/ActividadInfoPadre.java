package com.una.kinder.actividades.grupo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.actividades.ImageUtils;
import com.una.kinder.entidades.Usuario;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ActividadInfoPadre extends AppCompatActivity {

    private EditText cedula;
    private EditText nombre;
    private EditText apellido;
    private EditText numtel;
    private EditText contrasena;
    private EditText tipoUsuario;
    private Button actualizarBtn;
    private SimpleDraweeView profileImage;

    private FirebaseAuth auth;
    private ProgressDialog progressDialogRegistrar;
    private ProgressDialog progressDialog;
    private FirebaseDatabase database;
    private FirebaseStorage firebaseStorage;

    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_info_padre);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupInputs();
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        progressDialog = new ProgressDialog(this);
        firebaseStorage = FirebaseStorage.getInstance();

        cargarInformacionUsuario();
    }

    private void setupInputs() {
        profileImage = (SimpleDraweeView) findViewById(R.id.profile_image);
        cedula = (EditText) findViewById(R.id.input_cedula_perfil);
        nombre = (EditText) findViewById(R.id.input_nombre);
        apellido = (EditText) findViewById(R.id.input_apellido);
        numtel = (EditText) findViewById(R.id.input_telefono);
        contrasena = (EditText) findViewById(R.id.input_password);
        tipoUsuario = (EditText) findViewById((R.id.input_tipo));
        actualizarBtn = (Button) findViewById(R.id.btn_actualizar);
    }

    private void mostrarProgressDialog(String title, String text, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(text);
        progressDialog.setCanceledOnTouchOutside(cancelable);
        progressDialog.show();
    }

    private void cargarInformacionUsuario() {
        idUsuario = getIntent().getExtras().getString("id");
        mostrarProgressDialog("Cargando usuario", "Por favor espere mientras cargamos la información", false);
        database.getReference(FirebaseUtils.CHILD_USUARIO).child(idUsuario).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                cedula.setText(usuario.getIndexCedula());
                nombre.setText(usuario.getNombre());
                apellido.setText(usuario.getApellidos());
                numtel.setText(usuario.getNumeroTelefono());
                tipoUsuario.setText(usuario.getTipo());
                profileImage.setImageURI(usuario.getUrlImagen());
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void mostrarMensajeToast(CharSequence mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
