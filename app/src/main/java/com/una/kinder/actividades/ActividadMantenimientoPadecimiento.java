package com.una.kinder.actividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.una.kinder.R;
import com.una.kinder.entidades.Evento;
import com.una.kinder.entidades.Padecimiento;

import java.util.HashMap;
import java.util.Map;

public class ActividadMantenimientoPadecimiento extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference itemRef;
    String index;
    Button cancelar;
    Button modificar;
    Button eliminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_mantenimiento_padecimiento);
        database= FirebaseDatabase.getInstance();


        final EditText tipo = (EditText) findViewById(R.id.edit_tipo_pad);
        final EditText descripcion = (EditText) findViewById(R.id.edit_desc_pad);

        modificar=(Button) findViewById(R.id.btn_modificarPad);
        eliminar= (Button) findViewById(R.id.btn_eliminarPad);
        cancelar= (Button) findViewById(R.id.btn_cancelarPad);

        Intent intent= getIntent();

        index=intent.getStringExtra("indexPadecimiento");
        String tipoI= intent.getStringExtra("tipo");
        final String descripcionI= intent.getStringExtra("descripcion");

        itemRef= database.getReference("padecimientos").child(index);

        tipo.setText(tipoI);
        descripcion.setText(descripcionI);

        modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ndescripcion=descripcion.getText().toString();
                String ntipo=tipo.getText().toString();

                Map<String, Object> padUdate = new HashMap<>();
                padUdate.put("descripcion",ndescripcion);
                padUdate.put("tipo",ntipo);
                itemRef.updateChildren(padUdate);
                finish();

            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemRef.removeValue();
                finish();
            }
        });

    }
}
