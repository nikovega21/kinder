package com.una.kinder.actividades;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.una.kinder.R;

public class ActividadAcerca extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_acerca);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
