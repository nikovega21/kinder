package com.una.kinder.actividades.grupo;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.entidades.Grupo;

public class ActividadInfoGrupo extends AppCompatActivity {

    private Button button_listado_ninos;
    private Button button_listado_padres;
    private String id;
    private String nombreGrupo;

    private FirebaseDatabase database;
    private DatabaseReference ref;

    private Grupo grupo;
    private EditText nombreText;
    private EditText numeroText;
    private EditText descripcionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_info_grupo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id = getIntent().getExtras().getString("id");
        nombreGrupo=getIntent().getExtras().getString("nombre");

        button_listado_ninos = findViewById(R.id.button_listado_ninos);
        button_listado_ninos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadInfoGrupo.this, ActividadNinosGrupo.class);
                intent.putExtra("id", id);
                intent.putExtra("nombre", nombreGrupo);
                startActivity(intent);
            }
        });
        button_listado_padres = findViewById(R.id.button_listado_padres);
        button_listado_padres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadInfoGrupo.this, ActividadPadresGrupo.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });

        setupInputs();

        database = FirebaseDatabase.getInstance();
        ref = database.getReference(FirebaseUtils.CHILD_GRUPO);
        cargarInformacion();
    }

    private void setupInputs() {
        nombreText = findViewById(R.id.nombre_grupo);
        numeroText = findViewById(R.id.numero_grupo);
        descripcionText = findViewById(R.id.descripcion_grupo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_actualizar_grupo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actualizar_grupo:
                if (validarCampos()) {
                    grupo.setNombre(nombreText.getText().toString());
                    grupo.setNumero(Integer.parseInt(numeroText.getText().toString()));
                    grupo.setDescripcion(descripcionText.getText().toString());
                    actualizarInformacion();
                } else {
                    Toast.makeText(this, "Ningún campo puede estar vacío", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validarCampos() {
        return !(TextUtils.isEmpty(nombreText.getText().toString())) && TextUtils.isDigitsOnly(numeroText.getText().toString()) && !(TextUtils.isEmpty(descripcionText.getText().toString()));
    }

    private void cargarInformacion() {
        ref.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    grupo = dataSnapshot.getValue(Grupo.class);
                    nombreText.setText(grupo.getNombre());
                    numeroText.setText(grupo.getNumero() + "");
                    descripcionText.setText(grupo.getDescripcion());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void actualizarInformacion() {
        ref.child(id).setValue(grupo).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(ActividadInfoGrupo.this, "Grupo actualizado", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ActividadInfoGrupo.this, "No se pudo actualizar el grupo", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
