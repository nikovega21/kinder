package com.una.kinder.actividades;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.una.kinder.R;
import com.una.kinder.entidades.Post;

import org.w3c.dom.Text;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.github.ponnamkarthik.richlinkpreview.MetaData;
import io.github.ponnamkarthik.richlinkpreview.ResponseListener;
import io.github.ponnamkarthik.richlinkpreview.RichPreview;

public class ActividadCrearPost extends AppCompatActivity implements OnConnectionFailedListener {
    BottomNavigationView bottomNavigationView;
    FrameLayout frameContenido;
    private final int PERMISO_ACCEDER_FOTOS = 0;
    private final int PERMISO_ACCEDER_UBICACION = 1;
    private boolean[] permisos = {false, false};
    private GoogleApiClient mGoogleApiClient;
    private EditText textNuevoPost;

    private final String TIPO_POST_TEXTO = "texto";
    private final String TIPO_POST_MAP = "map";
    private final String TIPO_POST_LINK = "link";
    private final String TIPO_POST_IMAGEN = "imagen";

    private String tipoPost = "texto";
    private Uri imageData;
    private String mapData;
    private Map<String, String> linkData;

    //Progreso alert
    ProgressDialog progressDialog;

    //Elementos de inicio
    private TextView nombreUsuario;
    private SimpleDraweeView imagenUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_crear_post);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        frameContenido = findViewById(R.id.frame_contenido_nuevo_post);
        bottomNavigationView = findViewById(R.id.agregar_items_post);
        verificarPermisos();//Verifica los permisos en versiones posteriores a Android 6.0
        bottomNavigationView.setOnNavigationItemSelectedListener(bottomNavigationListener);

        //Conexion con la api de google places
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
        nombreUsuario = findViewById(R.id.nombre_usuario_nuevo_post);
        imagenUsuario = findViewById(R.id.imagen_usuario_nuevo_post);
        textNuevoPost = findViewById(R.id.text_nuevo_de_post);

    }

    @Override
    protected void onStart() {
        super.onStart();
        imagenUsuario.setImageURI(Uri.parse(this.getIntent().getExtras().getString("imagen")));
        nombreUsuario.setText(this.getIntent().getExtras().getString("nombre"));
    }

    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavigationListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            // Handle menu_inferior view item clicks here.
            frameContenido.removeAllViews();//Limpia el frame donde se ve la vista previa de los contenidos
            int id = item.getItemId();
            Intent intent;
            if (id == R.id.navigation_add_image_post) {
                if (permisos[PERMISO_ACCEDER_FOTOS]) {
                    intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Seleccionar una imagen"), Constantes.CODIGO_OBTENER_IMAGEN);
                } else {
                    ActivityCompat.requestPermissions(ActividadCrearPost.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISO_ACCEDER_FOTOS);
                }

            } else if (id == R.id.navigation_add_link_post) {
                solicitarLink();
            } else if (id == R.id.navigation_add_location_post) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(ActividadCrearPost.this), Constantes.CODIGO_OBTENER_UBICACION);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }
    };

    private void verificarPermisos() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permisos[PERMISO_ACCEDER_FOTOS] = false;
        } else {
            permisos[PERMISO_ACCEDER_FOTOS] = true;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISO_ACCEDER_FOTOS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permisos[PERMISO_ACCEDER_FOTOS] = true;
                } else {
                    Toast.makeText(ActividadCrearPost.this, "Debe de brindar el permiso de lectura de imagenes para continuar", Toast.LENGTH_SHORT).show();
                    permisos[PERMISO_ACCEDER_FOTOS] = false;
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        frameContenido.removeAllViews();
        switch (requestCode) {
            case Constantes.CODIGO_OBTENER_IMAGEN:
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this, "Imagen agregada", Toast.LENGTH_SHORT).show();
                    Uri imageUri = data.getData();
                    try {
                        ImageView imageView = new ImageView(this);
                        imageView.setImageBitmap(ImageUtils.comprimirImagenToBitmapSize(imageUri, 50, 400, this));
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        imageView.setAdjustViewBounds(true);
                        limpiarData();
                        frameContenido.addView(imageView);
                        imageData = imageUri;
                        tipoPost = TIPO_POST_IMAGEN;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case Constantes.CODIGO_OBTENER_UBICACION:
                if (resultCode == RESULT_OK) {
                    final Place place = PlacePicker.getPlace(data, this);
                    FragmentManager fm = getSupportFragmentManager();
                    SupportMapFragment supportMapFragment = SupportMapFragment.newInstance();
                    supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            googleMap.getUiSettings().setRotateGesturesEnabled(false);
                            googleMap.getUiSettings().setTiltGesturesEnabled(false);
                            googleMap.getUiSettings().setZoomGesturesEnabled(false);
                            googleMap.getUiSettings().setMapToolbarEnabled(true);
                            googleMap.getUiSettings().setScrollGesturesEnabled(false);
                            Marker locationMarker = googleMap.addMarker(new MarkerOptions().position(place.getLatLng()).title("Mi posicion").flat(true));
                            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.builder().target(place.getLatLng()).zoom(15).build()));
                        }
                    });
                    int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getResources().getDisplayMetrics());
                    frameContenido.setLayoutParams(new LinearLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height));
                    fm.beginTransaction().replace(frameContenido.getId(), supportMapFragment).commit();
                    limpiarData();
                    mapData = place.getLatLng().latitude + "," + place.getLatLng().longitude;
                    tipoPost = TIPO_POST_MAP;
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crear_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.enviarPost:
                switch (tipoPost) {
                    case TIPO_POST_TEXTO:
                        if (textNuevoPost.getText().toString().trim().length() > 0) {
                            Post post = new Post();
                            post.setTexto(textNuevoPost.getText().toString());
                            post.setFecha(new SimpleDateFormat("EEE, d MMM yyyy hh:mm a").format(new Date()).toString());
                            post.setTipo(TIPO_POST_TEXTO);
                            post.setData(new HashMap<String, String>());
                            post.setIndexProfesor(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            FirebaseDatabase.getInstance().getReference(FirebaseUtils.CHILD_POST).push().setValue(post).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(ActividadCrearPost.this, "Enviado", Toast.LENGTH_LONG).show();
                                        finish();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(this, "No ingreso nada", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case TIPO_POST_IMAGEN:
                        try {
                            progressDialog = new ProgressDialog(ActividadCrearPost.this);
                            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            progressDialog.setMax(100);
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.setTitle("Subiendo imagen");
                            progressDialog.setMessage("Por favor espera mientras subimos la imagen");
                            final byte[] bytes = ImageUtils.comprimirImagenToBytes(imageData, 100, this);
                            final UploadTask uploadTask = FirebaseStorage.getInstance().getReference(FirebaseUtils.STORAGE_IMAGENES_POST).child(FirebaseUtils.randomString(40)).putBytes(bytes);
                            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    uploadTask.cancel();
                                    progressDialog.cancel();
                                }
                            });
                            progressDialog.show();
                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.i("Upload Fail", "Upload Fail");
                                }
                            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    Double progreso = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                    progressDialog.setProgress(progreso.intValue());
                                }
                            }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        Post post = new Post();
                                        post.setTexto(textNuevoPost.getText().toString());
                                        post.setFecha(new SimpleDateFormat("EEE, d MMM yyyy hh:mm a").format(new Date()));
                                        post.setTipo(TIPO_POST_IMAGEN);
                                        Map<String, String> data = new HashMap<>();
                                        data.put("image_url", task.getResult().getDownloadUrl().toString());
                                        post.setData(data);
                                        post.setIndexProfesor(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                        FirebaseDatabase.getInstance().getReference(FirebaseUtils.CHILD_POST).push().setValue(post).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                progressDialog.dismiss();
                                                Toast.makeText(ActividadCrearPost.this, "Post creado", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        });

                                    }
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case TIPO_POST_MAP:
                        Post post = new Post();
                        post.setTexto(textNuevoPost.getText().toString());
                        post.setFecha(new SimpleDateFormat("EEE, d MMM yyyy hh:mm a").format(new Date()));
                        post.setTipo(TIPO_POST_MAP);
                        Map<String, String> data = new HashMap<>();
                        data.put("lat", mapData.split(",")[0]);
                        data.put("lng", mapData.split(",")[1]);
                        post.setData(data);
                        post.setIndexProfesor(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        FirebaseDatabase.getInstance().getReference(FirebaseUtils.CHILD_POST).push().setValue(post).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ActividadCrearPost.this, "Enviado", Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            }
                        });
                        break;
                    case TIPO_POST_LINK:
                        Post postLink = new Post();
                        postLink.setTexto(textNuevoPost.getText().toString());
                        postLink.setFecha(new SimpleDateFormat("EEE, d MMM yyyy hh:mm a").format(new Date()));
                        postLink.setTipo(TIPO_POST_LINK);
                        postLink.setData(linkData);

                        postLink.setIndexProfesor(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        FirebaseDatabase.getInstance().getReference(FirebaseUtils.CHILD_POST).push().setValue(postLink).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ActividadCrearPost.this, "Enviado", Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            }
                        });
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void limpiarData() {
        imageData = null;
        mapData = null;
        linkData = null;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public void solicitarLink() {
        frameContenido.removeAllViews();
        final EditText link = new EditText(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Ingrese el link que desea adjuntar al post:");
        link.setHint("Enlace del post");
        link.selectAll();
        builder.setView(link);

        builder.setCancelable(true);
        builder.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                limpiarData();
                tipoPost = TIPO_POST_LINK;

                RichPreview richPreview = new RichPreview(new ResponseListener() {
                    @Override
                    public void onData(MetaData metaData) {
                        linkData = new HashMap<>();
                        linkData.put("titulo", metaData.getTitle());
                        linkData.put("image_url", metaData.getImageurl());
                        linkData.put("descripcion", metaData.getDescription());
                        linkData.put("sitio", metaData.getSitename());
                        linkData.put("url", metaData.getUrl());
                        linkData.put("mediatype", metaData.getMediatype());

                        View v = LayoutInflater.from(ActividadCrearPost.this).inflate(R.layout.item_rich_link, null);
                        SimpleDraweeView imagen = v.findViewById(R.id.imagen_link);
                        imagen.setImageURI(Uri.parse(metaData.getImageurl()));
                        TextView titulo = v.findViewById(R.id.titulo_link);
                        titulo.setText((metaData.getSitename() + " | " + metaData.getTitle()));
                        TextView descripcion = v.findViewById(R.id.descripcion_link);
                        descripcion.setText(metaData.getDescription());
                        frameContenido.addView(v);
                    }

                    @Override
                    public void onError(Exception e) {
                        //handle error
                        Toast.makeText(ActividadCrearPost.this, "El link ingresado no es valido", Toast.LENGTH_SHORT).show();
                    }
                });
                richPreview.getPreview(link.getText().toString());
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(ActividadCrearPost.this, "Se canceló la inserción del link", Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
