package com.una.kinder.actividades;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.adapters.AdapterNinosPadecimiento;
import com.una.kinder.entidades.Nino;
import com.una.kinder.entidades.Usuario;

import java.util.ArrayList;

public class ActividadListadoNinosPadecimiento extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseDatabase database;
    //DatabaseReference myRef;

    RecyclerView reciclador;
    ArrayList<Nino> listaNinos;
    AdapterNinosPadecimiento adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_listado_ninos_padecimiento);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Padecimientos - Niños");
        }

        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        if(auth.getCurrentUser()!=null) {

            database.getReference(FirebaseUtils.CHILD_USUARIO).addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    Usuario usuario = dataSnapshot.child(auth.getCurrentUser().getUid()).getValue(Usuario.class);

                    reciclador = (RecyclerView) findViewById(R.id.recycler_nino_pad);
                    reciclador.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reciclador.getContext(), LinearLayoutManager.VERTICAL);
                    reciclador.addItemDecoration(dividerItemDecoration);

                    listaNinos = new ArrayList<>();
                    adapter = new AdapterNinosPadecimiento(listaNinos);
                    reciclador.setAdapter(adapter);

                    if (usuario.getTipo().equals( FirebaseUtils.TIPO_USUARIO_PADRE)) {

                        database.getReference(FirebaseUtils.CHILD_PADRE).child(auth.getCurrentUser().getUid()).child("ninos").addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                String nino = dataSnapshot.getKey();
                                database.getReference(FirebaseUtils.CHILD_NINO).child(nino).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        boolean flag = false;
                                        Nino nino = dataSnapshot.getValue(Nino.class);
                                        if (nino != null) {
                                            for (int i = 0; i < listaNinos.size(); i++){
                                                if (listaNinos.get(i).getLlave() == nino.getLlave()) {
                                                    listaNinos.set(i, nino);
                                                    adapter.notifyDataSetChanged();
                                                    i = 100;
                                                    flag = true;
                                                }
                                            }
                                            if (flag == false) {
                                                listaNinos.add(nino);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                        reciclador.setAdapter(adapter);
                    } else {
                        database.getReference(FirebaseUtils.CHILD_NINO).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                listaNinos.clear();
                                for (DataSnapshot item : dataSnapshot.getChildren()) {
                                    Nino nino = item.getValue(Nino.class);
                                    listaNinos.add(nino);
                                }
                                adapter = new AdapterNinosPadecimiento(listaNinos);
                                reciclador.setAdapter(adapter);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
