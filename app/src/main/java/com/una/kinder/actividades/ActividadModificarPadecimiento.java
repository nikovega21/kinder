package com.una.kinder.actividades;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.una.kinder.R;

import java.util.HashMap;
import java.util.Map;

public class ActividadModificarPadecimiento extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference itemRef;


    ProgressDialog progressDialog;

    String index;
    String tipoI;
    String descripcionI;

    Button cancelar;
    Button modificar;
    Button eliminar;
    EditText tipo;
    EditText descripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_modificar_padecimiento);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        database = FirebaseDatabase.getInstance();

        progressDialog = new ProgressDialog(this);

        tipo = (EditText) findViewById(R.id.edit_tipo_pad);
        descripcion = (EditText) findViewById(R.id.edit_desc_pad);
        modificar = (Button) findViewById(R.id.btn_modificarPad);
        eliminar = (Button) findViewById(R.id.btn_eliminarPad);
        cancelar = (Button) findViewById(R.id.btn_cancelarPad);

        Intent intent = getIntent();

        index = intent.getStringExtra("indexPadecimiento");
        tipoI = intent.getStringExtra("tipo");
        descripcionI = intent.getStringExtra("descripcion");

        itemRef = database.getReference(FirebaseUtils.CHILD_PADECIMIENTOS).child(index);

        tipo.setText(tipoI);
        descripcion.setText(descripcionI);

        modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validar()) {
                    MensajeConfirmar(itemRef, 2);
                }
            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MensajeConfirmar(itemRef, 1);
            }
        });
    }

    private void eliminar(DatabaseReference ref) {
        mostrarProgressDialog("Eliminando Padecimiento", "Espere por favor", false);
        ref.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Mensaje("Padecimiento eliminado", Toast.LENGTH_SHORT);
                } else {
                    Mensaje("Ocurrió un error, intenta nuevamente", Toast.LENGTH_SHORT);
                }
                progressDialog.dismiss();
                finish();
            }
        });
    }

    private void modificar(DatabaseReference ref) {
        mostrarProgressDialog("Modificando Padecimiento", "Espere por favor", false);
        String ndescripcion = descripcion.getText().toString();
        String ntipo = tipo.getText().toString();

        Map<String, Object> padUdate = new HashMap<>();
        padUdate.put("descripcion", ndescripcion);
        padUdate.put("tipo", ntipo);
        ref.updateChildren(padUdate).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Mensaje("Padecimiento Actualizado", Toast.LENGTH_SHORT);
                } else {
                    Mensaje("Ocurrió un error, intenta nuevamente", Toast.LENGTH_SHORT);
                }
                progressDialog.dismiss();
                finish();
            }
        });
    }

    private void mostrarProgressDialog(String title, String text, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(text);
        progressDialog.setCanceledOnTouchOutside(cancelable);
        progressDialog.show();
    }

    private void MensajeConfirmar(final DatabaseReference ref, final int code) {
        View v1 = getWindow().getDecorView().getRootView();
        AlertDialog.Builder builder1 = new AlertDialog.Builder(v1.getContext());
        builder1.setMessage(R.string.title_confirmar_guardar);
        builder1.setCancelable(true);
        builder1.setPositiveButton(R.string.title_confirmar_Ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                switch (code) {
                    case 1:
                        eliminar(ref);
                        break;
                    case 2:
                        modificar(ref);
                        break;
                }
            }
        });
        builder1.setNegativeButton(R.string.title_cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public boolean validar() {
        if (tipo.getText().toString().trim().length() > 0 &&
                descripcion.getText().toString().trim().length() > 0) {
            return true;
        }
        Mensaje("Por favor completa todos los espacios", Toast.LENGTH_SHORT);
        return false;
    }

    public void Mensaje(String msg, int duracion) {
        Toast.makeText(getApplicationContext(), msg, duracion).show();
    }
}
