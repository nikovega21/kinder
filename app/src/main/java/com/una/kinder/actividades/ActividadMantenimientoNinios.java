package com.una.kinder.actividades;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.grupo.ActividadNinosGrupo;
import com.una.kinder.adapters.AdapterNinoGrupo;
import com.una.kinder.adapters.AdapterNinos;
import com.una.kinder.adapters.AdapterNotificaciones;
import com.una.kinder.entidades.Nino;
import com.una.kinder.entidades.Notificacion;

import java.util.ArrayList;
import java.util.HashMap;

public class ActividadMantenimientoNinios extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference databaseReference;
    RecyclerView recycler;
    ArrayList<Nino> lista;
    AdapterNinos adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_actividad_mantenimiento_ninios);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btn_mas_mant_ninios);
        cargarNinos();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intento =new Intent(getApplicationContext(),ActividadAgregarDatosNino.class);
                startActivity(intento);
            }
        });
    }


    public void Mensajea(String msg){getSupportActionBar().setTitle(msg);}
    public void cargarNinos(){
        auth = FirebaseAuth.getInstance();
        database= FirebaseDatabase.getInstance();
        if(auth.getCurrentUser()!=null) {

            databaseReference = database.getReference(FirebaseUtils.CHILD_PADRE).child(auth.getCurrentUser().getUid()).child("ninos");
            recycler = (RecyclerView) findViewById(R.id.recycler_ninios);
            recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            lista = new ArrayList<>();
            adapter = new AdapterNinos(lista, this);

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler.getContext(), LinearLayoutManager.VERTICAL);
            recycler.addItemDecoration(dividerItemDecoration);
            database.getReference(FirebaseUtils.CHILD_PADRE).child(auth.getCurrentUser().getUid()).child("ninos").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String nino = dataSnapshot.getKey();
                    database.getReference(FirebaseUtils.CHILD_NINO).child(nino).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            boolean flag =false;
                            Nino nino = dataSnapshot.getValue(Nino.class);
                            if(nino!= null) {

                                for (int i = 0; i < lista.size(); i++)
                                    if (lista.get(i).getLlave() == nino.getLlave()) {
                                        lista.set(i,nino);
                                        adapter.notifyDataSetChanged();
                                        i = 100;
                                        flag = true;
                                    }
                                    if(flag==false){
                                        lista.add(nino);
                                        adapter.notifyDataSetChanged();
                                    }
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            recycler.setAdapter(adapter);
        }
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                xMark = ContextCompat.getDrawable(ActividadMantenimientoNinios.this, R.drawable.ic_delete_forever24dp);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) ActividadMantenimientoNinios.this.getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                AdapterNinos adapter = (AdapterNinos) recycler.getAdapter();
                adapter.remove(swipedPosition);

            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                if (viewHolder.getAdapterPosition() == -1) {
                    return;
                }

                if (!initiated) {
                    init();
                }

                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth() + 35;
                int intrinsicHeight = xMark.getIntrinsicWidth() + 35;

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public long getAnimationDuration(RecyclerView recyclerView, int animationType, float animateDx, float animateDy) {
                return DEFAULT_SWIPE_ANIMATION_DURATION;
            }
        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleCallback);
        mItemTouchHelper.attachToRecyclerView(recycler);
    }

    public void Mensaje(String msg){
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();}
}