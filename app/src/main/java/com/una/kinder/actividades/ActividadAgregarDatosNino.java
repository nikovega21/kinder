package com.una.kinder.actividades;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.una.kinder.R;
import com.una.kinder.entidades.Nino;
import com.una.kinder.entidades.Padre;
import com.una.kinder.entidades.Usuario;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ActividadAgregarDatosNino extends AppCompatActivity {

   private EditText edtNombre;
   private EditText edtApellidos;
   private EditText edtFechaNacimiento;
   private EditText edTelefonoEmergencia;
   private EditText edtEdadNino;
   private SimpleDraweeView profileImage;
   private ProgressDialog progressDialog;
   private FirebaseStorage firebaseStorage;
   private String llave;
    Nino nino;
    Padre padre;

   private FirebaseDatabase database;
   private DatabaseReference reference;
   private DatabaseReference notificacionReference;
   private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState);getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_actividad_nino);
        llave="";
        nino=null;
        progressDialog = new ProgressDialog(this);
        firebaseStorage = FirebaseStorage.getInstance();
        iniciarFirebaseDependecias();
        setUpinputs();
        cargarPadre();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_nuevo_nino, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.guardar_nuevo_nino:
                if(validarDatosNino())
                    MensajeConfirmar();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void MensajeConfirmar(){
            View v1 = getWindow().getDecorView().getRootView();
            AlertDialog.Builder builder1 = new AlertDialog.Builder( v1.getContext());
            builder1.setMessage(R.string.title_confirmar_guardar);
            builder1.setCancelable(true);
            builder1.setPositiveButton(R.string.title_confirmar_Ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            registrarMenor();
                        } });
            builder1.setNegativeButton(R.string.title_cancelar, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alert11 = builder1.create();
            alert11.show();
    }

    //metodo necesario para conectarse con firebase y permitir agregar valores a la base de datos
    private void iniciarFirebaseDependecias(){
        database=FirebaseDatabase.getInstance();
        reference=database.getReference(FirebaseUtils.CHILD_NINO);
        auth=FirebaseAuth.getInstance();
    }

    private void setUpinputs(){
        profileImage = (SimpleDraweeView) findViewById(R.id.profile_image_nino);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)
                        .setMinCropWindowSize(500, 500)
                        .start(ActividadAgregarDatosNino.this);
            }
        });
        edtNombre=findViewById(R.id.input_nombre_nino);
        edtApellidos=findViewById(R.id.input_apellido_nino);
        edtFechaNacimiento=findViewById(R.id.input_fecha_nac_nino);
        edTelefonoEmergencia=findViewById(R.id.input_telefono_nino);
        edtEdadNino =findViewById(R.id.input_edad_nino);
    }

    private void registrarMenor() {
        auth=FirebaseAuth.getInstance();
        if(llave.isEmpty())
         llave=database.getReference(FirebaseUtils.CHILD_NINO).push().getKey();

         tomarDatos();
         nino.setLlave(llave);
        mostrarProgressDialog("Guardando la informacion", "Espere por favor", false);
        database.getReference(FirebaseUtils.CHILD_NINO).child(llave).setValue(nino).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    if(padre!=null){
                    HashMap<String, Boolean> hash = padre.getNinos() == null ? new HashMap<String, Boolean>() : padre.getNinos();
                    hash.put(llave, true);
                    padre.setNinos(hash);
                    FirebaseUtils.database.getReference(FirebaseUtils.CHILD_PADRE).child(FirebaseUtils.auth.getCurrentUser().getUid()).setValue(padre).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful())
                                Mensaje("Agregado exitosamente", 0);

                            progressDialog.dismiss();
                        }
                    });
                }
                    finish();
                }else{
                    progressDialog.dismiss();
                    Mensaje("Imposible agregar por el momento",Toast.LENGTH_LONG);
                }
            }
        });
    }
    private void tomarDatos(){
       if(nino==null)
           nino=new Nino();
       nino.setNombre(  edtNombre.getText().toString().trim());
       nino.setApellidos( edtApellidos.getText().toString().trim());
       nino.setEdad(Integer.parseInt(edtEdadNino.getText().toString()));
       nino.setFechaNacimiento( edtFechaNacimiento.getText().toString());
       nino.setTelefonoEmergencia( edTelefonoEmergencia.getText().toString());
       nino.setIndexPadre(FirebaseUtils.usuarioActual().getUid());
       modoInicial();
    }
    public void modoInicial(){
        edtNombre.setText("");
        edtApellidos.setText("");
        edtEdadNino.setText("");
        edtFechaNacimiento.setText("");
        edTelefonoEmergencia.setText("");
    }

    private boolean validarDatosNino(){
        if(edtNombre.getText().toString().trim().length()>2 &&
         edtApellidos.getText().toString().trim().length()> 2 &&
         edtEdadNino.getText().toString().trim().length()> 0 &&
         edtFechaNacimiento.getText().toString().trim().length()> 2 &&
         edTelefonoEmergencia.getText().toString().trim().length()> 2){
            return true;
        }
        Mensaje("Por favor completa todos los espacios",Toast.LENGTH_SHORT);
        return false;
    }

    private FirebaseUser usuarioActual() {
        return auth.getInstance().getCurrentUser();
    }

    public void Mensaje(String msg,int duracion){
        Toast.makeText(getApplicationContext(), msg, duracion).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {//Obtiene la imagen recortada del activity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //Picasso.get().load(newImageUri).into(profileImage);
                subirNuevaImagen(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void subirNuevaImagen(final Uri newImageUri) {
        mostrarProgressDialog("Subiendo imagen", "Por favor espere miestras subimos la nueva imagen", false);
        try {
            byte[] bytes = ImageUtils.comprimirImagenToBytesSize(newImageUri, 50,350,this);
            UploadTask uploadTask = firebaseStorage.getReference(FirebaseUtils.STORAGE_IMAGENES_NINOS).child(FirebaseUtils.randomString(FirebaseUtils.TAMANO_URL_IMAGEN) + ".jpg").putBytes(bytes);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Mensaje("Ocurrió un error al subir la imagen, intenta nuevamente",0);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if(nino==null)
                        nino=new Nino();
                    if(llave.isEmpty())
                        llave=database.getReference(FirebaseUtils.CHILD_NINO).push().getKey();
                    nino.setLlave(llave);
                    nino.setUrlImagen(taskSnapshot.getDownloadUrl().toString());
                    database.getReference(FirebaseUtils.CHILD_NINO).child(llave).setValue(nino).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> taskActualizarUsuario) {
                                if (taskActualizarUsuario.isSuccessful()) {
                                    setImagenPerfilNino();
                                    Mensaje("Imagen actualizada con éxito",Toast.LENGTH_SHORT);
                                } else {
                                    Mensaje("Ocurrió un error, intenta nuevamente",Toast.LENGTH_SHORT);
                                }
                                progressDialog.dismiss();
                    }
                        });
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            Mensaje("Ocurrió un error, intenta nuevamente",1);
        }
    }
    private void mostrarProgressDialog(String title, String text, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(text);
        progressDialog.setCanceledOnTouchOutside(cancelable);
        progressDialog.show();
    }

    private void setImagenPerfilNino() {
        mostrarProgressDialog("Cargando usuario", "Por favor espere mientras cargamos su información", false);
        database.getReference(FirebaseUtils.CHILD_NINO).child(llave).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Nino n = dataSnapshot.getValue(Nino.class);
                if(n!=null) {
                    profileImage.setImageURI(n.getUrlImagen());
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void cargarPadre(){
        database.getInstance().getReference(FirebaseUtils.CHILD_PADRE).child(FirebaseUtils.auth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                padre=(Padre) dataSnapshot.getValue(Padre.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}