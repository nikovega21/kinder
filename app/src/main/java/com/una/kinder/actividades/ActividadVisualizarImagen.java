package com.una.kinder.actividades;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.una.kinder.R;

public class ActividadVisualizarImagen extends AppCompatActivity {

    private SimpleDraweeView imagenActual;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_visualizar_imagen);
        imagenActual = findViewById(R.id.imagen_visualizar_fullscreen);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        imagenActual.setImageURI(Uri.parse(this.getIntent().getExtras().getString("imagen_url")));
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        if(android.os.Build.VERSION.SDK_INT >= 22) {
            // Do something fancy
            getWindow().setStatusBarColor(Color.BLACK);
        }
    }
}
