package com.una.kinder.actividades.articulos;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.actividades.ImageUtils;
import com.una.kinder.entidades.Articulo;
import com.una.kinder.entidades.Nino;
import com.una.kinder.entidades.Padre;

import java.io.IOException;
import java.util.HashMap;

public class ActividadAgregarArticulo extends AppCompatActivity {
    private EditText edtNombre;
    private EditText edtCantidad;
    private SimpleDraweeView profileImage;
    private ProgressDialog progressDialog;
    private FirebaseDatabase database;
    private FirebaseStorage firebaseStorage;
    private FirebaseAuth auth;
    private String indexNino;
    int requestCode;
    int resultCode;
    Intent data;
    boolean tieneImagen=false;
    Uri newImageUri;

    private Articulo articulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_agregar_articulo);

        progressDialog = new ProgressDialog(this);
        firebaseStorage = FirebaseStorage.getInstance();
        Intent i = getIntent();
        indexNino=(String)i.getSerializableExtra("id_nino");
        setUpinputs();
        tieneImagen=false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_nuevo_articulo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.guardar_nuevo_articulo:
                if(validarDatosArticulo())
                    MensajeConfirmar();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void MensajeConfirmar(){
        View v1 = getWindow().getDecorView().getRootView();
        AlertDialog.Builder builder1 = new AlertDialog.Builder( v1.getContext());
        builder1.setMessage(R.string.title_confirmar_guardar);
        builder1.setCancelable(true);
        builder1.setPositiveButton(R.string.title_confirmar_Ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                registrarArticulo();
            } });
        builder1.setNegativeButton(R.string.title_cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void setUpinputs(){
        profileImage = (SimpleDraweeView) findViewById(R.id.image_articulo_agregar);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)
                        .setMinCropWindowSize(500, 500)
                        .start(ActividadAgregarArticulo.this);
            }
        });
        edtNombre=findViewById(R.id.input_nombre_articulo);
        edtCantidad =findViewById(R.id.input_cantidad_articulo);
    }

    private void registrarArticulo() {
        auth=FirebaseAuth.getInstance();

        if(!tieneImagen)
            casoGuardarArticulo("default");
        else{
            casoGuardarImagen();
        }
    }

    public void casoGuardarArticulo(String urlImagen){
        tomarDatos();
        if(urlImagen!="default")
            articulo.setUrlImagen(urlImagen);
        //llena los datos del articulo
        mostrarProgressDialog("Guardando la informacion", "Espere por favor", false);
        String llave_articulo = FirebaseUtils.database.getReference(FirebaseUtils.CHILD_ARTICULO).child(indexNino).push().getKey();
        articulo.setLlave(llave_articulo);
        FirebaseUtils.database.getReference(FirebaseUtils.CHILD_ARTICULO).child(indexNino).child(llave_articulo).setValue(articulo).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Mensaje("Registrado correctamente", 1);
                    progressDialog.dismiss();
                    finish();
                }
            }
        });
    }

    public void casoGuardarImagen(){
        subirNuevaImagen(newImageUri);
    }
    private void tomarDatos(){
         articulo=new Articulo();
         articulo.setIndexNino(indexNino);
         articulo.setNombre( edtNombre.getText().toString());
         articulo.setCantidad(edtCantidad.getText().toString());
         modoInicial();
    }


    public void modoInicial(){
        edtNombre.setText("");
        edtCantidad.setText("");
        tieneImagen=false;
        newImageUri=null;
    }

    private boolean validarDatosArticulo(){
        if(edtNombre.getText().toString().trim().length()>2 &&
                edtCantidad.getText().toString().trim().length()> 0){
            return true;
        }
        Mensaje("Por favor completa todos los espacios",Toast.LENGTH_SHORT);
        return false;
    }

    public void Mensaje(String msg,int duracion){
        Toast.makeText(getApplicationContext(), msg, duracion).show();
    }



    private void subirNuevaImagen(final Uri newImageUri) {
        mostrarProgressDialog("Subiendo imagen", "Por favor espere miestras subimos la nueva imagen", false);
        try {
            byte[] bytes = ImageUtils.comprimirImagenToBytesSize(newImageUri, 50,350,this);
            UploadTask uploadTask = firebaseStorage.getReference(FirebaseUtils.STORAGE_IMAGENES_NINOS).child(FirebaseUtils.randomString(FirebaseUtils.TAMANO_URL_IMAGEN) + ".jpg").putBytes(bytes);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Mensaje("Ocurrió un error al subir la imagen, intenta nuevamente",0);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    casoGuardarArticulo(taskSnapshot.getDownloadUrl().toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            Mensaje("Ocurrió un error, intenta nuevamente",1);
        }
    }
    private void mostrarProgressDialog(String title, String text, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(text);
        progressDialog.setCanceledOnTouchOutside(cancelable);
        progressDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {//Obtiene la imagen recortada del activity
        tieneImagen=true;
        this.requestCode=requestCode;
        this.resultCode=resultCode;
        this.data=data;

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //Picasso.get().load(newImageUri).into(profileImage);
                newImageUri=result.getUri();
                setImagen();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

    private void setImagen() {
        profileImage.setImageURI(newImageUri);
    }
}