package com.una.kinder.actividades.grupo;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.adapters.AdapterNinoAgregar;
import com.una.kinder.adapters.AdapterNinoGrupo;
import com.una.kinder.entidades.Grupo;
import com.una.kinder.entidades.Nino;
import com.una.kinder.entidades.Notificacion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActividadNinosAgregar extends AppCompatActivity {
    private FirebaseDatabase database;
    private DatabaseReference ref;
    private String idGrupo;
    private RecyclerView reciclador;
    private String nombreGrupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_ninos_agregar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        database = FirebaseDatabase.getInstance();
        ref = database.getReference(FirebaseUtils.CHILD_GRUPO);
        idGrupo = getIntent().getExtras().getString("id");
        nombreGrupo= getIntent().getExtras().getString("nombre");
        reciclador = (RecyclerView) findViewById(R.id.nino_agregar_recycler);
        reciclador.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reciclador.getContext(), LinearLayoutManager.VERTICAL);
        reciclador.addItemDecoration(dividerItemDecoration);
        AdapterNinoAgregar adapter = new AdapterNinoAgregar(new ArrayList<Nino>());
        reciclador.setAdapter(adapter);
        cargarNinos();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_agregar_ninos_grupo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.agregar_ninos_grupo:
                if (((AdapterNinoAgregar) reciclador.getAdapter()).getAgregarNinos().size() > 0) {
                    agregarNinos();
                } else {
                    Toast.makeText(this, "Debe de seleccionar ninos para continuar", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void cargarNinos() {

        AdapterNinoAgregar adapter = new AdapterNinoAgregar(new ArrayList<Nino>());
        reciclador.setAdapter(adapter);
        database.getReference(FirebaseUtils.CHILD_NINO).orderByChild("grupos").equalTo(null).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Nino nino = dataSnapshot.getValue(Nino.class);
                ((AdapterNinoAgregar) reciclador.getAdapter()).agregar(nino);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Nino p0 = dataSnapshot.getValue(Nino.class);
                AdapterNinoAgregar adapter = ((AdapterNinoAgregar) reciclador.getAdapter());
                for (int i = 0; i < adapter.getNinos().size(); i++) {
                    if (adapter.get(i).getLlave() == p0.getLlave()) {
                        adapter.remove(i);
                        adapter.notifyItemRemoved(i);
                        adapter.notifyItemRangeChanged(i, adapter.getNinos().size());
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void agregarNinos() {
        final Map ninos = ((AdapterNinoAgregar) reciclador.getAdapter()).getAgregarNinos();
        ref.child(idGrupo).child("ninos").updateChildren(ninos).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    Map grupo = new HashMap<String, Boolean>();
                    grupo.put(idGrupo, true);

                    List<String> keys = new ArrayList<>(ninos.keySet());
                    for (String i : keys) {
                        database.getReference(FirebaseUtils.CHILD_NINO).child(i).child("grupos").updateChildren(grupo);
                        notificarApadre(i);
                    }
                    Toast.makeText(ActividadNinosAgregar.this, "Los niños se han agregado exitosamente", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(ActividadNinosAgregar.this, "Ocurrió un error inesperado", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void notificarApadre(String keyNino){
        FirebaseUtils.database.getReference(FirebaseUtils.CHILD_NINO).child(keyNino).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Nino nino=(Nino) dataSnapshot.getValue(Nino.class);
                String key=FirebaseUtils.database.getReference(FirebaseUtils.CHILD_NOTIFICACION).child(nino.getIndexPadre()).push().getKey();
                Notificacion nt=new Notificacion();
                nt.setAsunto(nombreGrupo+" Notifica");
                nt.setFecha(fechaActualString());
                nt.setImageCode(1);
                nt.setIndexUsuario(nino.getIndexPadre());
                nt.setMensaje(nino.getNombre()+" agregado al grupo exitosamente!");
                nt.setKey(key);
                FirebaseUtils.database.getReference(FirebaseUtils.CHILD_NOTIFICACION).child(nino.getIndexPadre()).child(key).setValue(nt);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public String fechaActualString(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy");
        String fecha= simpleDateFormat.format(date);
        return fecha;
    }
}
