package com.una.kinder.actividades;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.una.kinder.entidades.Notificacion;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by geekMQ on 3/26/2018.
 */

public class FirebaseUtils {


    public static FirebaseDatabase database = FirebaseDatabase.getInstance();
    public static DatabaseReference notificacionReference = database.getReference(FirebaseUtils.CHILD_NOTIFICACION);
    public static FirebaseAuth auth = FirebaseAuth.getInstance();
    private String tempUsuarioToken;

    final public static String BASE_KINDER = "kinder";
    final public static String CHILD_USUARIO = "Usuario";
    public static final String CHILD_GRUPO = "Grupo";
    final public static String CHILD_NOTIFICACION = "Notificacion";
    final public static String CHILD_PROFESOR = "Profesor";
    final public static String CHILD_PADRE = "Padre";
    final public static String CHILD_NINO = "Nino";
    final public static String CHILD_ARTICULO = "Articulo";
    final public static String CHILD_MEDICINA = "Medicina";
    final public static String CHILD_PADECIMIENTOS = "Padecimientos";
    final public static String CHILD_POST = "Post";
    final public static String TIPO_USUARIO_PROFESOR = "Profesor";
    final public static String TIPO_USUARIO_PADRE = "Padre";
    final public static String STORAGE_IMAGENES_PERFIL = "imagenes_perfil_usuario";
    final public static String STORAGE_IMAGENES_NINOS = "imagenes_perfil_ninos";
    final public static String STORAGE_IMAGENES_ARTICULOS = "imagenes_Articulos";
    final public static int TAMANO_URL_IMAGEN = 30;
    public static final String STORAGE_IMAGENES_POST = "posts_images";

    public static String randomString(int len) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-+";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static void enviarNotificacionAmiDispositivo(final Notificacion n) {
        final FirebaseUser user = usuarioActual();
        database.getReference(FirebaseUtils.CHILD_USUARIO).child(user.getUid()).child("token").setValue(obtenerToken()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    notificacionReference.child(user.getUid()).push().setValue(n);
                }
            }
        });
    }

    public static FirebaseUser usuarioActual() {
        return auth.getInstance().getCurrentUser();
    }

    public static String obtenerToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }
}
