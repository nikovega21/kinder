package com.una.kinder.actividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.una.kinder.R;
import com.una.kinder.fragmentos.FragmentoEventos;

public class ActividadAgregarEvento extends AppCompatActivity {
    public  static int REQUESTCODE=1234;
    public static int RESULT_OK=12;
    public static int RESULT_CANCELED=9;

    String extra_fecha;
    EditText name;
    EditText grupo;
    EditText hora;
    EditText lugar;
    EditText fecha;
    Button cancelar;
    Button aceptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_agregar_evento);
        extra_fecha = getIntent().getStringExtra("fecha");
        name = (EditText) findViewById(R.id.ediitText_name);
        grupo = (EditText) findViewById(R.id.editText_grupo);
        hora = (EditText) findViewById(R.id.editText_hora);
        lugar = (EditText) findViewById(R.id.editText_lugar);
        fecha=(EditText) findViewById(R.id.editText_fecha);
        fecha.setText(extra_fecha);
        cancelar=(Button) findViewById(R.id.btn_cancelar);
        aceptar= (Button) findViewById(R.id.btn_aceptar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        aceptar.setOnClickListener(new View.OnClickListener(){

            @Override

            public void onClick(View arg0) {
                if (validar()==true) {
                    Intent intento = new Intent(getApplicationContext(), FragmentoEventos.class);
                    intento.putExtra("actividad", name.getText().toString());
                    intento.putExtra("grupo", grupo.getText().toString());
                    intento.putExtra("fecha", fecha.getText().toString());
                    intento.putExtra("lugar", lugar.getText().toString());
                    intento.putExtra("hora", hora.getText().toString());

                    setResult(ActividadAgregarEvento.RESULT_OK, intento);
                    finish();
                }

            }

        });

        cancelar.setOnClickListener(new View.OnClickListener(){

            @Override

            public void onClick(View arg0) {
                Intent intento = new Intent(getApplicationContext(), FragmentoEventos.class);
                setResult(ActividadAgregarEvento.RESULT_CANCELED,intento);
                finish();

            }

        });
    }


    public boolean validar(){
        Log.i("VALIDAR", String.valueOf(name.getText().toString().trim().length()));
        Log.i("VALIDAR", String.valueOf(grupo.getText().toString().trim().length()));
        Log.i("VALIDAR", String.valueOf(lugar.getText().toString().trim().length()));
        Log.i("VALIDAR",String.valueOf( hora.getText().toString().trim().length()));
        if(name.getText().toString().trim().length()>2 &&
                grupo.getText().toString().trim().length()> 0 &&
                lugar.getText().toString().trim().length()> 2 &&
                hora.getText().toString().trim().length()> 2){
            return true;
        }
        Mensaje("Por favor completa todos los espacios", Toast.LENGTH_SHORT);
        return false;
    }

    public void Mensaje(String msg,int duracion){
        Toast.makeText(getApplicationContext(), msg, duracion).show();
    }
}
