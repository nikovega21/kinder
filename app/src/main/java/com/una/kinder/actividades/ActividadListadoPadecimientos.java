package com.una.kinder.actividades;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.adapters.AdapterPadecimiento;
import com.una.kinder.entidades.Padecimiento;

import java.util.ArrayList;

public class ActividadListadoPadecimientos extends AppCompatActivity {

    RecyclerView reciclador;
    ArrayList<Padecimiento> listaPadecimientos;
    AdapterPadecimiento adapter;
    String indexNino;
    FirebaseDatabase database;
    DatabaseReference myRef;
    FloatingActionButton fab;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_listado_padecimientos);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Padecimientos");
        }

        progressDialog = new ProgressDialog(this);

        database = FirebaseDatabase.getInstance();

        fab = (FloatingActionButton) findViewById(R.id.agregar_pad);

        Intent intent = getIntent();

        indexNino = intent.getStringExtra("indexNino");

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(getApplicationContext(), ActividadAgregarDatosPadecimiento.class);
                intento.putExtra("indexNino", indexNino);
                startActivityForResult(intento, ActividadAgregarDatosPadecimiento.REQUESTCODE);
            }
        });

        myRef = database.getReference(FirebaseUtils.CHILD_PADECIMIENTOS);
        listaPadecimientos = new ArrayList<>();
        this.reciclador = (RecyclerView) findViewById(R.id.recyclerPad);


        Query query = myRef.orderByChild("indexNino").equalTo(indexNino);
        query.keepSynced(true);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listaPadecimientos.clear();
                for (DataSnapshot item : dataSnapshot.getChildren()) {
                    Padecimiento padecimiento = item.getValue(Padecimiento.class);
                    listaPadecimientos.add(padecimiento);
                }
                adapter = new AdapterPadecimiento(listaPadecimientos);
                reciclador.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        reciclador.setLayoutManager(new LinearLayoutManager(this.getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new AdapterPadecimiento(listaPadecimientos);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reciclador.getContext(), LinearLayoutManager.VERTICAL);
        reciclador.addItemDecoration(dividerItemDecoration);
        reciclador.setAdapter(adapter);

    }

    private void mostrarProgressDialog(String title, String text, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(text);
        progressDialog.setCanceledOnTouchOutside(cancelable);
        progressDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ActividadAgregarDatosPadecimiento.REQUESTCODE) {
            if (resultCode == ActividadAgregarDatosPadecimiento.RESULT_OK) {

                mostrarProgressDialog("Agregando Padecimiento", "Espere por favor", false);
                String tipo = data.getStringExtra("tipo");
                String descripcion = data.getStringExtra("descripcion");
                String indexNino = data.getStringExtra("indexNino");

                String index = myRef.push().getKey();
                Padecimiento p = new Padecimiento(tipo, descripcion, indexNino, index);
                myRef.child(index).setValue(p).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Mensaje("Padecimiento Agregado", Toast.LENGTH_SHORT);
                        } else {
                            Mensaje("Ocurrió un error, intenta nuevamente", Toast.LENGTH_SHORT);
                        }
                        progressDialog.dismiss();
                    }
                });
            } else if (resultCode == ActividadAgregarDatosPadecimiento.RESULT_CANCELED) {

            }

        }
    }

    public void Mensaje(String msg, int duracion) {
        Toast.makeText(getApplicationContext(), msg, duracion).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
