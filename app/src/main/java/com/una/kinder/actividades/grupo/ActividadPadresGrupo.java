package com.una.kinder.actividades.grupo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.adapters.AdapterGrupo;
import com.una.kinder.adapters.AdapterNinoGrupo;
import com.una.kinder.adapters.AdapterPadreGrupo;
import com.una.kinder.entidades.Grupo;
import com.una.kinder.entidades.Nino;
import com.una.kinder.entidades.Padre;
import com.una.kinder.entidades.Usuario;

import java.util.ArrayList;

public class ActividadPadresGrupo extends AppCompatActivity {
    private RecyclerView.ItemDecoration itemDecoration;
    private RecyclerView reciclador;
    private AdapterPadreGrupo adapter;
    private FirebaseDatabase database;
    private DatabaseReference ref;
    private String idGrupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_padres_grupo);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.reciclador = (RecyclerView) findViewById(R.id.padre_group_recycler);

        database = FirebaseDatabase.getInstance();
        ref = database.getReference(FirebaseUtils.CHILD_GRUPO);
        idGrupo = getIntent().getExtras().getString("id");

        reciclador.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reciclador.getContext(), LinearLayoutManager.VERTICAL);
        reciclador.addItemDecoration(dividerItemDecoration);
        cargarPadres();
    }

    private void cargarPadres() {
        adapter = new AdapterPadreGrupo(new ArrayList<Usuario>());
        reciclador.setAdapter(adapter);
        ref.child(idGrupo).child("ninos").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String key = dataSnapshot.getKey();
                database.getReference(FirebaseUtils.CHILD_NINO).child(key).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Nino nino = dataSnapshot.getValue(Nino.class);
                        database.getReference(FirebaseUtils.CHILD_USUARIO).child(nino.getIndexPadre()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                                usuario.setIndexUsuario(dataSnapshot.getKey());
                                adapter.agregar(usuario);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                /*Nino p0 = dataSnapshot.getValue(Nino.class);
                AdapterNinoGrupo adapter = ((AdapterNinoGrupo) reciclador.getAdapter());
                for (int i = 0; i < adapter.getNinos().size(); i++) {
                    if (adapter.get(i).getLlave() == p0.getLlave()) {
                        adapter.remove(i);
                        adapter.notifyItemRemoved(i);
                        adapter.notifyItemRangeChanged(i, adapter.getNinos().size());
                    }
                }*/
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
