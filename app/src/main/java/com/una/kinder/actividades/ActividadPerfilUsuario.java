package com.una.kinder.actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.una.kinder.R;
import com.una.kinder.entidades.Usuario;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ActividadPerfilUsuario extends AppCompatActivity {

    private EditText cedula;
    private EditText nombre;
    private EditText apellido;
    private EditText numtel;
    private EditText correo;
    private EditText contrasena;
    private EditText tipoUsuario;
    private Button actualizarBtn;
    private SimpleDraweeView profileImage;

    private FirebaseAuth auth;
    private ProgressDialog progressDialogRegistrar;
    private ProgressDialog progressDialog;
    private FirebaseDatabase database;
    private FirebaseStorage firebaseStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_perfil_usuario);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupInputs();
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        progressDialog = new ProgressDialog(this);
        firebaseStorage = FirebaseStorage.getInstance();
        cargarInformacionUsuario();
    }

    private void setupInputs() {
        profileImage = (SimpleDraweeView) findViewById(R.id.profile_image);
        cedula = (EditText) findViewById(R.id.input_cedula_perfil);
        nombre = (EditText) findViewById(R.id.input_nombre);
        apellido = (EditText) findViewById(R.id.input_apellido);
        numtel = (EditText) findViewById(R.id.input_telefono);
        correo = (EditText) findViewById(R.id.input_email);
        contrasena = (EditText) findViewById(R.id.input_password);
        tipoUsuario = (EditText) findViewById((R.id.input_tipo));
        actualizarBtn = (Button) findViewById(R.id.btn_actualizar);

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)
                        .setMinCropWindowSize(500, 500)
                        .start(ActividadPerfilUsuario.this);
            }
        });

        actualizarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario usuario = new Usuario();
                usuario.setIndexCedula(cedula.getText().toString());
                usuario.setNombre(nombre.getText().toString());
                usuario.setApellidos(apellido.getText().toString());
                usuario.setNumeroTelefono(numtel.getText().toString());
                usuario.setCorreo(correo.getText().toString());
                usuario.setTipo(tipoUsuario.getText().toString());
                actualizarUsuario(usuario);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {//Obtiene la imagen recortada del activity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //Picasso.get().load(newImageUri).into(profileImage);
                subirNuevaImagen(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void mostrarProgressDialog(String title, String text, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(text);
        progressDialog.setCanceledOnTouchOutside(cancelable);
        progressDialog.show();
    }

    private void subirNuevaImagen(final Uri newImageUri) {
        mostrarProgressDialog("Subiendo imagen", "Por favor espere miestras subimos la nueva imagen", false);
        try {

            byte[] bytes = ImageUtils.comprimirImagenToBytesSize(newImageUri, 50,350,this);
            UploadTask uploadTask = firebaseStorage.getReference(FirebaseUtils.STORAGE_IMAGENES_PERFIL).child(FirebaseUtils.randomString(FirebaseUtils.TAMANO_URL_IMAGEN) + ".jpg").putBytes(bytes);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    mostrarMensajeToast("Ocurrió un error al subir la imagen, intenta nuevamente");
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Map userMap = new HashMap<>();
                    userMap.put("urlImagen", taskSnapshot.getDownloadUrl().toString());

                    database.getReference(FirebaseUtils.CHILD_USUARIO).child(auth.getCurrentUser().getUid()).updateChildren(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> taskActualizarUsuario) {
                            if (taskActualizarUsuario.isSuccessful()) {
                                mostrarMensajeToast("Imagen actualizada con éxito");
                            } else {
                                mostrarMensajeToast("Ocurrió un error, intenta nuevamente");
                            }
                            progressDialog.dismiss();
                        }
                    });
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            mostrarMensajeToast("Ocurrió un error, intenta nuevamente");
        }
    }

    private void cargarInformacionUsuario() {
        mostrarProgressDialog("Cargando usuario", "Por favor espere mientras cargamos su información", false);
        database.getReference(FirebaseUtils.CHILD_USUARIO).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Usuario usuario = dataSnapshot.child(auth.getCurrentUser().getUid()).getValue(Usuario.class);
                cedula.setText(usuario.getIndexCedula());
                nombre.setText(usuario.getNombre());
                apellido.setText(usuario.getApellidos());
                numtel.setText(usuario.getNumeroTelefono());
                correo.setText(auth.getCurrentUser().getEmail());
                tipoUsuario.setText(usuario.getTipo());
                profileImage.setImageURI(usuario.getUrlImagen());
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void actualizarUsuario(final Usuario usuario) {
        if (cedula.getText().toString().trim().length() > 0 &&
                nombre.getText().toString().trim().length() > 0 &&
                apellido.getText().toString().trim().length() > 0 &&
                numtel.getText().toString().trim().length() > 0 &&
                correo.getText().toString().trim().length() > 0 &&
                tipoUsuario.getText().toString().trim().length() > 0) {
            Map userMap = new HashMap<>();
            userMap.put("indexCedula", usuario.getIndexCedula());
            userMap.put("nombre", usuario.getNombre());
            userMap.put("apellidos", usuario.getApellidos());
            userMap.put("numeroTelefono", usuario.getNumeroTelefono());
            userMap.put("correo", usuario.getCorreo());
            userMap.put("tipo", usuario.getTipo());
            database.getReference(FirebaseUtils.CHILD_USUARIO).child(auth.getCurrentUser().getUid()).updateChildren(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> taskActualizarUsuario) {
                    if (taskActualizarUsuario.isSuccessful()) {
                        mostrarMensajeToast("Información actualizada con exito");
                    } else {
                        mostrarMensajeToast("Ocurrió un error, intenta nuevamente");
                    }
                    progressDialog.dismiss();
                }
            });

        } else {
            mostrarMensajeToast("Ningún campo puede estar vacío");
        }
    }
    private void mostrarMensajeToast(CharSequence mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
