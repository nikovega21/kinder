package com.una.kinder.actividades.articulos;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.adapters.AdapterArticulosNino;
import com.una.kinder.entidades.Articulo;
import com.una.kinder.entidades.Nino;
import java.util.ArrayList;

public class ActividadMantenimientoArticulos extends AppCompatActivity {

    String llaveNiño;
    ArrayList<Articulo> arrayArticulos;
    RecyclerView recicler;
    AdapterArticulosNino adapter;
    Nino nino;
    private SimpleDraweeView profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_mantenimiento_articulos);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recicler=(RecyclerView) findViewById(R.id.recycler_articulos);
        Intent i = getIntent();
        llaveNiño=(String)i.getSerializableExtra("id_nino");
        recicler.setLayoutManager(new LinearLayoutManager(this, GridLayoutManager.VERTICAL, false));

        //bottom line
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recicler.getContext(), GridLayoutManager.VERTICAL);
        recicler.addItemDecoration(dividerItemDecoration);
        arrayArticulos=new ArrayList<>();
        adapter=new AdapterArticulosNino(arrayArticulos,this);
        recicler.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                xMark = ContextCompat.getDrawable(ActividadMantenimientoArticulos.this, R.drawable.ic_delete_forever24dp);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) ActividadMantenimientoArticulos.this.getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                AdapterArticulosNino adapter = (AdapterArticulosNino) recicler.getAdapter();
                adapter.remove(swipedPosition);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                if (viewHolder.getAdapterPosition() == -1) {
                    return;
                }

                if (!initiated) {
                    init();
                }

                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth() + 35;
                int intrinsicHeight = xMark.getIntrinsicWidth() + 35;

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public long getAnimationDuration(RecyclerView recyclerView, int animationType, float animateDx, float animateDy) {
                return DEFAULT_SWIPE_ANIMATION_DURATION;
            }
        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleCallback);
        mItemTouchHelper.attachToRecyclerView(recicler);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btn_mas_articulos_nino);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), ActividadAgregarArticulo.class);
                i.putExtra("id_nino", llaveNiño);
                startActivity(i);

                Intent intento =new Intent(getApplicationContext(),ActividadAgregarArticulo.class);
                startActivity(intento);
            }
        });
        cargarNino();
        cargarArticulosNiño();
    }

    private void cargarNino() {
        FirebaseUtils.database.getInstance().getReference(FirebaseUtils.CHILD_NINO).child(llaveNiño).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                nino=(Nino) dataSnapshot.getValue(Nino.class);
                if(nino!=null) {
                    setImagenPerfilNino();
                    setTitle(nino.getNombre());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void setImagenPerfilNino() {
        profileImage = (SimpleDraweeView) findViewById(R.id.profile_image_nino_articulo);
        profileImage.setImageURI(nino.getUrlImagen());
    }

    private void cargarArticulosNiño() {

        FirebaseUtils.database.getInstance().getReference(FirebaseUtils.CHILD_ARTICULO).child(llaveNiño).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Articulo ar=(Articulo) dataSnapshot.getValue(Articulo.class);
                arrayArticulos.add(ar);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void Mensaje(String msg){
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();}
}
