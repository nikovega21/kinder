package com.una.kinder.actividades.grupo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.una.kinder.R;
import com.una.kinder.actividades.ActividadPrincipal;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.adapters.AdapterGrupo;
import com.una.kinder.entidades.Grupo;

import java.util.ArrayList;

public class ActividadMantenimientoGrupos extends AppCompatActivity {
    private RecyclerView.ItemDecoration itemDecoration;
    // private CalendarView calendario;
    private RecyclerView reciclador;
    private FloatingActionButton agregarButton;

    private FirebaseDatabase database;
    private DatabaseReference ref;
    private AdapterGrupo adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_mantenimiento_grupos);

        database = FirebaseDatabase.getInstance();
        ref = database.getReference(FirebaseUtils.CHILD_GRUPO);

        agregarButton = findViewById(R.id.agregar_nuevo_grupo);
        agregarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadMantenimientoGrupos.this, ActividadCrearGrupo.class);
                overridePendingTransition(R.animator.fade_in, R.animator.fade_out);
                startActivity(intent);

            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.reciclador = (RecyclerView) findViewById(R.id.group_recycler);

        reciclador.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reciclador.getContext(), LinearLayoutManager.VERTICAL);
        reciclador.addItemDecoration(dividerItemDecoration);

        //Inicializacion
        cargarGrupos();

        final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.refresh_groups);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cargarGrupos();
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void cargarGrupos() {
        adapter = new AdapterGrupo(new ArrayList<Grupo>());
        reciclador.setAdapter(adapter);
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Grupo grupo = dataSnapshot.getValue(Grupo.class);
                grupo.setIndexGrupo(dataSnapshot.getKey().toString());
                adapter.agregar(grupo);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
