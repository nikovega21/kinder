package com.una.kinder.actividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.una.kinder.R;
import com.una.kinder.entidades.Evento;

public class ActividadVerEvento extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference itemRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_ver_evento);

        database=FirebaseDatabase.getInstance();


        final EditText name = (EditText) findViewById(R.id.ediitText_name);
        final EditText grupo = (EditText) findViewById(R.id.editText_grupo);
        final EditText hora = (EditText) findViewById(R.id.editText_hora);
        final EditText lugar = (EditText) findViewById(R.id.editText_lugar);
        final EditText fecha=(EditText) findViewById(R.id.editText_fecha);
        Button modificar=(Button) findViewById(R.id.btn_modificar);
        Button cancelar= (Button) findViewById(R.id.btn_cancelar);
        Button eliminar=(Button) findViewById(R.id.btn_eliminar);


        Intent intent= getIntent();

        final String indexi=intent.getStringExtra("actividad");
        String tituloi= intent.getStringExtra("descripcion");
        String fechai= intent.getStringExtra("fecha");
        String lugari= intent.getStringExtra("lugar");
        String horai= intent.getStringExtra("hora");
        String grupoi=intent.getStringExtra("grupo");

        itemRef= database.getReference("eventos").child(indexi);

        name.setText(tituloi);
        grupo.setText(grupoi);
        hora.setText(horai);
        lugar.setText(lugari);
        fecha.setText(fechai);



        modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ndescripcion=name.getText().toString();
                String ngrupo=grupo.getText().toString();
                String nfecha= fecha.getText().toString();
                String nlugar=lugar.getText().toString();
                String nhora=hora.getText().toString();

                Evento n=new Evento( indexi, ndescripcion, nfecha,nlugar,nhora,ngrupo);
                itemRef.setValue(n);
                finish();;

            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 itemRef.removeValue();
                 finish();
            }
        });
    }
}
