package com.una.kinder.actividades;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.una.kinder.R;
import com.una.kinder.entidades.Nino;

import java.io.IOException;

public class ActividadModificarNino extends AppCompatActivity {

    FirebaseDatabase database;
    FirebaseAuth auth;
    Nino ninio;
    private EditText edtNombre;
    private EditText edtApellidos;
    private EditText edtFechaNacimiento;
    private EditText edTelefonoEmergencia;
    private EditText edtEdadNino;

    private SimpleDraweeView profileImage;
    private ProgressDialog progressDialog;
    private FirebaseStorage firebaseStorage;
    private String llave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_modificar_nino);getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent i = getIntent();
        String llave=(String)i.getSerializableExtra("id_nino");

        progressDialog = new ProgressDialog(this);
        firebaseStorage = FirebaseStorage.getInstance();

        setUpinputs();
        database=FirebaseDatabase.getInstance();
        auth=FirebaseAuth.getInstance();
        database.getReference(FirebaseUtils.CHILD_NINO).child(llave).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Nino nino=(Nino) dataSnapshot.getValue(Nino.class);
                ninio=nino;
                if(nino!=null) {
                    setImagenPerfilNino();
                    edtNombre.setText(nino.getNombre());
                    edtApellidos.setText(nino.getApellidos());
                    edtEdadNino.setText("" + nino.getEdad());
                    edtFechaNacimiento.setText(nino.getFechaNacimiento());
                    edTelefonoEmergencia.setText(nino.getTelefonoEmergencia());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void registrarMenor() {
        auth=FirebaseAuth.getInstance();
        tomarDatos();//actualiza el niño y mantiene lo anterior que no se modifico
        database.getReference(FirebaseUtils.CHILD_NINO).child(ninio.getLlave()).setValue(ninio).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Mensaje("Completado con exito");
                    finish();

                }else{
                    Mensaje("Imposible agregar por el momento");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_nuevo_nino, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.guardar_nuevo_nino:
                if(validarDatosNino()){
                    if(!noEditadoAun())
                       MensajeConfirmar();
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpinputs(){
        profileImage = (SimpleDraweeView) findViewById(R.id.profile_image_nino_modificar);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)
                        .setMinCropWindowSize(500, 500)
                        .start(ActividadModificarNino.this);
            }
        });

        edtNombre=findViewById(R.id.input_nombre_nino_modificar);
        edtApellidos=findViewById(R.id.input_apellido_nino_modificar);
        edtFechaNacimiento=findViewById(R.id.input_fecha_nac_nino_modificar);
        edTelefonoEmergencia=findViewById(R.id.input_telefono_nino_modificar);
        edtEdadNino =findViewById(R.id.input_edad_nino_modificar);
    }

    private boolean validarDatosNino(){
        if(edtNombre.getText().toString().trim().length()>2 &&
                edtApellidos.getText().toString().trim().length()> 2 &&
                edtEdadNino.getText().toString().trim().length()> 0 &&
                edtFechaNacimiento.getText().toString().trim().length()> 2 &&
                edTelefonoEmergencia.getText().toString().trim().length()> 2){
            return true;
        }
        Mensaje("Por favor completa todos los espacios");
        return false;
    }
    private boolean noEditadoAun(){
        if(edtNombre.getText().toString().equals(ninio.getNombre()) &&
                edtApellidos.getText().toString().equals(ninio.getApellidos()) &&
                Integer.parseInt(edtEdadNino.getText().toString())==ninio.getEdad() &&
                edtFechaNacimiento.getText().toString().equals(ninio.getFechaNacimiento())&&
                edTelefonoEmergencia.getText().toString().equals(ninio.getTelefonoEmergencia())) {
            Mensaje("No se ha editado ningun valor aún");
            return true;
        }else
        {
            return false;
        }


    }

    private Nino tomarDatos(){

        ninio.setNombre(  edtNombre.getText().toString().trim());
        ninio.setApellidos( edtApellidos.getText().toString().trim());
        ninio.setEdad(Integer.parseInt(edtEdadNino.getText().toString().trim()));
        ninio.setFechaNacimiento( edtFechaNacimiento.getText().toString());
        ninio.setTelefonoEmergencia( edTelefonoEmergencia.getText().toString());

        return ninio;
    }

    public void MensajeConfirmar(){
        View v1 = getWindow().getDecorView().getRootView();
        AlertDialog.Builder builder1 = new AlertDialog.Builder( v1.getContext());
        builder1.setMessage(R.string.title_confirmar_guardar);
        builder1.setCancelable(true);
        builder1.setPositiveButton(R.string.title_confirmar_Ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                registrarMenor();
            } });
        builder1.setNegativeButton(R.string.title_cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {//Obtiene la imagen recortada del activity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //Picasso.get().load(newImageUri).into(profileImage);
                subirNuevaImagen(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    private void subirNuevaImagen(final Uri newImageUri) {
        mostrarProgressDialog("Subiendo imagen", "Por favor espere miestras subimos la nueva imagen", false);
        try {
            byte[] bytes = ImageUtils.comprimirImagenToBytesSize(newImageUri, 50,350,this);
            UploadTask uploadTask = firebaseStorage.getReference(FirebaseUtils.STORAGE_IMAGENES_NINOS).child(FirebaseUtils.randomString(FirebaseUtils.TAMANO_URL_IMAGEN) + ".jpg").putBytes(bytes);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Mensaje("Ocurrió un error al subir la imagen, intenta nuevamente");
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    ninio.setUrlImagen(taskSnapshot.getDownloadUrl().toString());
                    database.getReference(FirebaseUtils.CHILD_NINO).child(ninio.getLlave()).setValue(ninio).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> taskActualizarUsuario) {
                            if (taskActualizarUsuario.isSuccessful()) {
                                setImagenPerfilNino();
                                Mensaje("Imagen actualizada con éxito");
                            } else {
                                Mensaje("Ocurrió un error, intenta nuevamente");
                            }
                            progressDialog.dismiss();
                        }
                    });
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            Mensaje("Ocurrió un error, intenta nuevamente");
        }
    }
    private void mostrarProgressDialog(String title, String text, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(text);
        progressDialog.setCanceledOnTouchOutside(cancelable);
        progressDialog.show();
    }

    private void setImagenPerfilNino() {
                    profileImage.setImageURI(ninio.getUrlImagen());
                    progressDialog.dismiss();
}
    public void Mensaje(String msg){
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();}
}
