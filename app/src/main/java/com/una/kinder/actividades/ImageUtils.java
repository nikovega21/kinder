package com.una.kinder.actividades;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import com.squareup.okhttp.internal.Util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;

public class ImageUtils {

    public static byte[] comprimirImagenToBytes(Uri newImageUri, int quality, Context context) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), newImageUri);
        File file = new File(getRealPathFromURI(newImageUri, context));
        Bitmap compressed = new Compressor(context).setMaxHeight((int) (bitmap.getHeight() * (500.0 / bitmap.getWidth()))).setMaxWidth(500).setQuality(quality).compressToBitmap(file);
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        compressed.compress(Bitmap.CompressFormat.JPEG, quality, byteArray);
        byte[] bytes = byteArray.toByteArray();
        return bytes;
    }

    public static Bitmap comprimirImagenToBitmap(Uri newImageUri, int quality, Context context) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), newImageUri);
        File file = new File(getRealPathFromURI(newImageUri, context));
        Bitmap compressed = new Compressor(context).setMaxHeight((int) (bitmap.getHeight() * (500.0 / bitmap.getWidth()))).setMaxWidth(500).setQuality(quality).compressToBitmap(file);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        compressed.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream);
        return compressed;
    }

    public static byte[] comprimirImagenToBytesSize(Uri newImageUri, int quality, double width, Context context) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), newImageUri);
        File file = new File(getRealPathFromURI(newImageUri, context));
        Bitmap compressed = new Compressor(context).setMaxHeight((int) (bitmap.getHeight() * (width / bitmap.getWidth()))).setMaxWidth((int) width).setQuality(quality).compressToBitmap(file);
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        compressed.compress(Bitmap.CompressFormat.JPEG, quality, byteArray);
        byte[] bytes = byteArray.toByteArray();
        return bytes;
    }

    public static Bitmap comprimirImagenToBitmapSize(Uri newImageUri, int quality, double width, Context context) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), newImageUri);
        File file = new File(getRealPathFromURI(newImageUri, context));
        Bitmap compressed = new Compressor(context).setMaxHeight((int) (bitmap.getHeight() * (width / bitmap.getWidth()))).setMaxWidth((int) width).setQuality(quality).compressToBitmap(file);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        compressed.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream);
        return compressed;
    }

    public static Bitmap bytesToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {
        Cursor cursor = context.getContentResolver().query(contentURI,
                null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        }
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}
