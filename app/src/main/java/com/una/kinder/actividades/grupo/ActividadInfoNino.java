package com.una.kinder.actividades.grupo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.actividades.ImageUtils;
import com.una.kinder.entidades.Nino;

import java.io.IOException;

public class ActividadInfoNino extends AppCompatActivity {

    FirebaseDatabase database;
    FirebaseAuth auth;
    Nino ninio;
    private EditText edtNombre;
    private EditText edtApellidos;
    private EditText edtFechaNacimiento;
    private EditText edTelefonoEmergencia;
    private EditText edtEdadNino;

    private SimpleDraweeView profileImage;
    private ProgressDialog progressDialog;
    private FirebaseStorage firebaseStorage;
    private String llave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_info_nino);getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent i = getIntent();
        String llave=(String)i.getSerializableExtra("id_nino");

        progressDialog = new ProgressDialog(this);
        firebaseStorage = FirebaseStorage.getInstance();

        setUpinputs();
        database=FirebaseDatabase.getInstance();
        auth=FirebaseAuth.getInstance();
        database.getReference(FirebaseUtils.CHILD_NINO).child(llave).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Nino nino=(Nino) dataSnapshot.getValue(Nino.class);
                ninio=nino;
                if(nino!=null) {
                    setImagenPerfilNino();
                    edtNombre.setText(nino.getNombre());
                    edtApellidos.setText(nino.getApellidos());
                    edtEdadNino.setText("" + nino.getEdad());
                    edtFechaNacimiento.setText(nino.getFechaNacimiento());
                    edTelefonoEmergencia.setText(nino.getTelefonoEmergencia());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    private void setUpinputs(){
        profileImage = (SimpleDraweeView) findViewById(R.id.profile_image_nino_modificar);

        edtNombre=findViewById(R.id.input_nombre_nino_modificar);
        edtApellidos=findViewById(R.id.input_apellido_nino_modificar);
        edtFechaNacimiento=findViewById(R.id.input_fecha_nac_nino_modificar);
        edTelefonoEmergencia=findViewById(R.id.input_telefono_nino_modificar);
        edtEdadNino =findViewById(R.id.input_edad_nino_modificar);
    }
    private void setImagenPerfilNino() {
                    profileImage.setImageURI(ninio.getUrlImagen());
                    progressDialog.dismiss();
}
    public void Mensaje(String msg){
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();}
}
