package com.una.kinder.actividades;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.grupo.ActividadMantenimientoGrupos;
import com.una.kinder.entidades.Usuario;
import com.una.kinder.fragmentos.FragmentoEventos;
import com.una.kinder.fragmentos.FragmentoNotificaciones;
import com.una.kinder.fragmentos.FragmentoPrincipal;

public class ActividadPrincipal extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //Fragmentos
    private FragmentoPrincipal fragmentoPrincipal;
    private FragmentoNotificaciones fragmentoNotificaciones;
    private FragmentoEventos fragmentoEventos;
    private FrameLayout frameLayout;
    private String tipoUsuario = "Padre";

    //Menus y toolbars
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private BottomNavigationView navigation;

    //Firebase instances
    private FirebaseAuth auth;

    //Objetos de pantalla
    private ProgressDialog progressDialog;

    private Usuario usuarioActual;
    private SimpleDraweeView imagenUsuarioPerfil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        //Inicializacion de toolbar de la app
        setupToolbar();

        //Inicializacion del menu lateral navigationDrawer
        setupNavigationDrawer();

        ////Inicializacion del bottom menu_inferior
        setupNavigationBottom();

        //Inicializacion de fragmentos de la app
        setupFragmentos();

        //Fragmento predeterminado
        setFragmento(fragmentoPrincipal, getFragmentManager().findFragmentById(R.id.frame_principal));
        auth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        imagenUsuarioPerfil = (SimpleDraweeView) findViewById(R.id.profile_image_header_menu);


        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        sensorManager.registerListener(lightSensorEventListener, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);

        ////Verficacion de usuario logueado
        verificarUsuario();
    }

    SensorEventListener lightSensorEventListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            // TODO Auto-generated method stub
            if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                final float currentReading = event.values[0];
                if (currentReading < 100) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                } else
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }

    };

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void verificarUsuario() {
        Intent intent;
        if (auth.getCurrentUser() == null) {
            intent = new Intent(this, ActividadLogin.class);
            startActivity(intent);
            finish();
        } else if (auth.getCurrentUser().isEmailVerified() == false) {
            intent = new Intent(this, ActividadLogin.class);
            startActivity(intent);
            finish();
        } else {
            initInfoUser();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_acerca_de:
                intent = new Intent(this, ActividadAcerca.class);
                startActivity(intent);
                break;
            case R.id.action_settings:
                intent = new Intent(this, ActividadAjustes.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }

    private void initInfoUser() {
        FirebaseDatabase.getInstance().getReference(FirebaseUtils.CHILD_USUARIO).child(auth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Usuario user = dataSnapshot.getValue(Usuario.class);
                if (user != null) {
                    if (user.getTipo().equals("Profesor"))
                        hideNavigatioDrawerItem(1);
                    else {
                        hideNavigatioDrawerItem(0);
                    }
                    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                    View headerView = navigationView.getHeaderView(0);

                    ((TextView) headerView.findViewById(R.id.nombre_usuario_header_menu)).setText((user.getNombre() + " " + user.getApellidos()));
                    Uri uri = Uri.parse(user.getUrlImagen());
                    ((SimpleDraweeView) headerView.findViewById(R.id.profile_image_header_menu)).setImageURI(uri);
                    ((TextView) headerView.findViewById(R.id.email_usuario_header_menu)).setText(user.getCorreo());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void mostrarProgressDialogLogout() {
        progressDialog.setTitle("Cerrando sesión");
        progressDialog.setMessage("Por favor espera, mientras cerramos la sesión");
        progressDialog.show();
    }

    private void logoutOfApp() {
        mostrarProgressDialogLogout();
        auth.signOut();
        progressDialog.dismiss();
        verificarUsuario();
    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void setupNavigationDrawer() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupNavigationBottom() {
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void setupFragmentos() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        frameLayout = (FrameLayout) findViewById(R.id.frame_principal);
        fragmentoPrincipal = new FragmentoPrincipal();
        fragmentoNotificaciones = new FragmentoNotificaciones();
        fragmentoEventos = new FragmentoEventos();

        fragmentTransaction.add(R.id.frame_principal, fragmentoPrincipal);
        fragmentTransaction.add(R.id.frame_principal, fragmentoNotificaciones);
        fragmentTransaction.add(R.id.frame_principal, fragmentoEventos);
        //fragmentTransaction.add(R.id.frame_principal, fragmentoMensajes);

        fragmentTransaction.hide(fragmentoNotificaciones);
        fragmentTransaction.hide(fragmentoEventos);
        //fragmentTransaction.hide(fragmentoMensajes);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle menu_inferior view item clicks here.
        int id = item.getItemId();
        Intent intent;
        if (id == R.id.nav_ajustes) {
            intent = new Intent(ActividadPrincipal.this, ActividadAjustes.class);
            startActivity(intent);
        } else if (id == R.id.nav_perfil) {
            intent = new Intent(ActividadPrincipal.this, ActividadPerfilUsuario.class);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            logoutOfApp();
        } else if (id == R.id.nav_nino) {
            Intent i = new Intent(getApplicationContext(), ActividadMantenimientoNinios.class);
            startActivity(i);
        } else if (id == R.id.nav_padecimiento) {
            Intent i = new Intent(getApplicationContext(), ActividadListadoNinosPadecimiento.class);
            startActivity(i);
        } else if (id == R.id.nav_grupos) {
            Intent i = new Intent(getApplicationContext(), ActividadMantenimientoGrupos.class);
            startActivity(i);
        } else if (id == R.id.nav_crear_recordatorio_medicina) {
            intent = new Intent(Intent.ACTION_INSERT).setData(CalendarContract.Events.CONTENT_URI);
            startActivity(intent);
        } else if (id == R.id.nav_padecimiento) {
            Intent i = new Intent(getApplicationContext(), ActividadVistaPadecimientos.class);
            startActivity(i);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragmentoActual = getFragmentManager().findFragmentById(R.id.frame_principal);
            switch (item.getItemId()) {
                case R.id.navigation_inicio:
                    setFragmento(fragmentoPrincipal, fragmentoActual);
                    return true;
                case R.id.navigation_eventos:
                    setFragmento(fragmentoEventos, fragmentoActual);
                    return true;
                case R.id.navigation_notificaciones:
                    setFragmento(fragmentoNotificaciones, fragmentoActual);
                    return true;
            }
            return false;
        }
    };

    private void setFragmento(Fragment fragmento, Fragment fragmentoActual) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        if (fragmento == fragmentoPrincipal) {

            fragmentTransaction.hide(fragmentoNotificaciones);
            fragmentTransaction.hide(fragmentoEventos);
            //fragmentTransaction.hide(fragmentoMensajes);

        } else if (fragmento == fragmentoNotificaciones) {

            fragmentTransaction.hide(fragmentoPrincipal);
            fragmentTransaction.hide(fragmentoEventos);
            //fragmentTransaction.hide(fragmentoMensajes);

        } else if (fragmento == fragmentoEventos) {

            fragmentTransaction.hide(fragmentoPrincipal);
            fragmentTransaction.hide(fragmentoNotificaciones);
            //fragmentTransaction.hide(fragmentoMensajes);

        }
        /*else if(fragmento == fragmentoMensajes){
            fragmentTransaction.hide(fragmentoPrincipal);
            fragmentTransaction.hide(fragmentoNotificaciones);
            fragmentTransaction.hide(fragmentoEventos);

        }*/
        fragmentTransaction.show(fragmento);
        fragmentTransaction.commit();
    }

    private void hideNavigatioDrawerItem(int tipo) {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        if (tipo == 1) {
            nav_Menu.findItem(R.id.nav_nino).setVisible(false);
            nav_Menu.findItem(R.id.nav_grupos).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.nav_nino).setVisible(true);
            nav_Menu.findItem(R.id.nav_grupos).setVisible(false);
        }

    }
}
