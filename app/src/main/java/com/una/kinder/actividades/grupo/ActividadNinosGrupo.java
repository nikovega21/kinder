package com.una.kinder.actividades.grupo;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.adapters.AdapterGrupo;
import com.una.kinder.adapters.AdapterNinoAgregar;
import com.una.kinder.adapters.AdapterNinoGrupo;
import com.una.kinder.adapters.AdapterPadreGrupo;
import com.una.kinder.entidades.Grupo;
import com.una.kinder.entidades.Nino;
import com.una.kinder.entidades.Usuario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ActividadNinosGrupo extends AppCompatActivity {
    private RecyclerView reciclador;
    private AdapterNinoGrupo adapter;
    private FirebaseDatabase database;
    private DatabaseReference ref;
    private String idGrupo;
    private String nombreGrupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_ninos_grupo);

        database = FirebaseDatabase.getInstance();
        ref = database.getReference(FirebaseUtils.CHILD_GRUPO);
        idGrupo = getIntent().getExtras().getString("id");
        nombreGrupo = getIntent().getExtras().getString("nombre");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupInputs();
        setupRecyclerview();
        cargarNinos();
    }

    private void setupInputs() {
        FloatingActionButton agregarButton = findViewById(R.id.agregar_nino_grupo);
        agregarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadNinosGrupo.this, ActividadNinosAgregar.class);
                intent.putExtra("id", idGrupo);
                intent.putExtra("nombre", nombreGrupo);

                startActivity(intent);

            }
        });
    }

    private void cargarNinos() {
        adapter = new AdapterNinoGrupo(new ArrayList<Nino>());
        reciclador.setAdapter(adapter);
        ref.child(idGrupo).child("ninos").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String nino = dataSnapshot.getKey();
                database.getReference(FirebaseUtils.CHILD_NINO).child(nino).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Nino nino = dataSnapshot.getValue(Nino.class);
                        if (nino.getGrupos().containsKey(idGrupo)) {
                            AdapterNinoGrupo adapter = ((AdapterNinoGrupo) reciclador.getAdapter());
                            for (int i = 0; i < adapter.getNinos().size(); i++) {
                                if (adapter.get(i).getLlave().equals(nino.getLlave())) {
                                    adapter.remove(i);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                            adapter.agregar(nino);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                /*Nino p0 = dataSnapshot.getValue(Nino.class);
                AdapterNinoGrupo adapter = ((AdapterNinoGrupo) reciclador.getAdapter());
                for (int i = 0; i < adapter.getNinos().size(); i++) {
                    if (adapter.get(i).getLlave() == p0.getLlave()) {
                        adapter.remove(i);
                        adapter.notifyItemRemoved(i);
                        adapter.notifyItemRangeChanged(i, adapter.getNinos().size());
                    }
                }*/
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setupRecyclerview() {
        reciclador = (RecyclerView) findViewById(R.id.nino_group_recycler);
        reciclador.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reciclador.getContext(), LinearLayoutManager.VERTICAL);
        reciclador.addItemDecoration(dividerItemDecoration);
        adapter = new AdapterNinoGrupo(new ArrayList<Nino>());
        reciclador.setAdapter(adapter);
        reciclador.setHasFixedSize(true);
        reciclador.setItemAnimator(new DefaultItemAnimator());

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                xMark = ContextCompat.getDrawable(ActividadNinosGrupo.this, R.drawable.ic_delete_forever24dp);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) ActividadNinosGrupo.this.getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                AdapterNinoGrupo adapter = (AdapterNinoGrupo) reciclador.getAdapter();
                Nino nino = (Nino) adapter.get(swipedPosition);
                deleteNino(nino.getLlave(), swipedPosition);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                if (viewHolder.getAdapterPosition() == -1) {
                    return;
                }

                if (!initiated) {
                    init();
                }

                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth() + 35;
                int intrinsicHeight = xMark.getIntrinsicWidth() + 35;

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public long getAnimationDuration(RecyclerView recyclerView, int animationType, float animateDx, float animateDy) {
                return DEFAULT_SWIPE_ANIMATION_DURATION;
            }
        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleCallback);
        mItemTouchHelper.attachToRecyclerView(reciclador);
    }

    private void deleteNino(final String id, final int position) {

        ref.child(idGrupo).child("ninos").child(id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(ActividadNinosGrupo.this, "Niño eliminado", Toast.LENGTH_SHORT).show();
                    adapter.remove(position);
                    database.getReference(FirebaseUtils.CHILD_NINO).child(id).child("grupos").child(idGrupo).removeValue();
                } else {
                    Toast.makeText(ActividadNinosGrupo.this, task.getException().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
