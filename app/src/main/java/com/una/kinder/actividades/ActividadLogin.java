package com.una.kinder.actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.una.kinder.R;

public class ActividadLogin extends AppCompatActivity {
    private FirebaseAuth auth;

    private ProgressDialog progressDialog;

    private Button loginBtn;
    private Button signupBtn;
    private EditText inputEmail;
    private EditText inputPassword;
    private int TAM_MIN_CONTRASENA = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_login);
        progressDialog = new ProgressDialog(this);
        auth = FirebaseAuth.getInstance();
        setupLoginActivity();
    }

    private void setupLoginActivity() {
        loginBtn = findViewById(R.id.btn_login);
        signupBtn = findViewById(R.id.btn_sign_up);
        inputEmail = findViewById(R.id.input_email);
        inputPassword = findViewById(R.id.input_password);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(inputEmail.getText().toString().length() <= TAM_MIN_CONTRASENA) || !(inputPassword.getText().toString().length() <= TAM_MIN_CONTRASENA)) {
                    loginToApp(inputEmail.getText().toString(), inputPassword.getText().toString());
                } else {
                    mensajeToast("Debe de llenar todos los campos");
                }
            }
        });
        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActividadRegistrarse.class);
                startActivity(intent);
            }
        });
    }

    public void loginToApp(String email, String password) {
        mostrarProgressDialogLogin();
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    if (auth.getCurrentUser().isEmailVerified()) {
                        progressDialog.dismiss();
                        Intent intent = new Intent(getApplicationContext(), ActividadPrincipal.class);
                        startActivity(intent);
                        finish();
                    }else{
                        progressDialog.dismiss();
                        mensajeToast("Primero verifique su dirección de correo elétronico.");
                    }
                } else {
                    progressDialog.hide();
                    mensajeToast("La autenticación ha fallado, intenta nuevamente.");

                }
            }
        });
    }

    public void mostrarProgressDialogLogin() {
        progressDialog.setTitle("Iniciando sesión");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Estamos comprobando tu indentidad por favor espere");
        progressDialog.show();
    }

    public void mensajeToast(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
