 package com.una.kinder.actividades;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.ActividadAgregarPadecimientos;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.adapters.AdapterPadecimiento;
import com.una.kinder.entidades.Padecimiento;
import java.util.ArrayList;


 public class ActividadVerPadecimiento extends AppCompatActivity {

    private RecyclerView reciclador;
    ArrayList<Padecimiento> listaPadecimientos;
    AdapterPadecimiento adapter;
    String indexNino;
    FirebaseDatabase database;
    DatabaseReference myRef;
    FloatingActionButton fab;


    // alambramos el Button


    //Programamos el evento onclick


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_ver_padecimiento);
        database = FirebaseDatabase.getInstance();

        fab = (FloatingActionButton) findViewById(R.id.agregar_pad);

        Intent intent= getIntent();

        indexNino= intent.getStringExtra("indexNino");

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(getApplicationContext(), ActividadAgregarPadecimientos.class);
                intento.putExtra("indexNino", indexNino);
                startActivityForResult(intento, ActividadAgregarPadecimientos.REQUESTCODE);
            }
        });

        myRef = database.getReference(FirebaseUtils.CHILD_PADECIMIENTOS);
        listaPadecimientos = new ArrayList<>();
        this.reciclador = (RecyclerView) findViewById(R.id.recyclerPad);



        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listaPadecimientos.clear();
                for (DataSnapshot item : dataSnapshot.getChildren()) {
                    Padecimiento padecimiento= item.getValue(Padecimiento.class);
                    listaPadecimientos.add(padecimiento);
                }
                adapter = new AdapterPadecimiento(listaPadecimientos);
                reciclador.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        reciclador.setLayoutManager(new LinearLayoutManager(this.getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new AdapterPadecimiento(listaPadecimientos);
        reciclador.setAdapter(adapter);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ActividadAgregarPadecimientos.REQUESTCODE) {
            if (resultCode == ActividadAgregarPadecimientos.RESULT_OK) {

                String tipo = data.getStringExtra("tipo");
                String descripcion = data.getStringExtra("descripcion");
                String indexNino=data.getStringExtra("indexNino");

                String index = myRef.push().getKey();
                Padecimiento p = new Padecimiento( tipo, descripcion,indexNino,index);
                myRef.child(index).setValue(p);
            } else if (resultCode == ActividadAgregarPadecimientos.RESULT_CANCELED) {

            }

        }
    }
}
