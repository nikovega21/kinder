package com.una.kinder.actividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.una.kinder.R;

public class ActividadAgregarPadecimientos extends AppCompatActivity {
    public  static int REQUESTCODE=134;
    public static int RESULT_OK=120;
    public static int RESULT_CANCELED=90;


    EditText tipo;
    EditText descripcion;
    Button cancelar;
    Button aceptar;
    String indexNino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_agregar_padecimiento);

        tipo=(EditText) findViewById(R.id.edit_tipo_pad);
        descripcion=(EditText) findViewById(R.id.edit_desc_pad);
        cancelar=(Button) findViewById(R.id.button_cancelar);
        aceptar= (Button) findViewById(R.id.button_aceptar);

        indexNino = getIntent().getStringExtra("indexNino");



        aceptar.setOnClickListener(new View.OnClickListener(){

            @Override

            public void onClick(View arg0) {
                if (validar()==true) {
                    Intent intento = new Intent(getApplicationContext(), ActividadVerPadecimiento.class);
                    intento.putExtra("tipo", tipo.getText().toString());
                    intento.putExtra("descripcion", descripcion.getText().toString());
                    intento.putExtra("indexNino", indexNino);
                    setResult(ActividadAgregarPadecimientos.RESULT_OK, intento);
                    finish();
                }

            }

        });

        cancelar.setOnClickListener(new View.OnClickListener(){

            @Override

            public void onClick(View arg0) {
                Intent intento = new Intent(getApplicationContext(), ActividadVerPadecimiento.class);
                setResult(ActividadAgregarPadecimientos.RESULT_CANCELED,intento);
                finish();

            }

        });
    }


    public boolean validar(){
        Log.i("VALIDAR", String.valueOf(tipo.getText().toString().trim().length()));
        Log.i("VALIDAR", String.valueOf(descripcion.getText().toString().trim().length()));

        if(tipo.getText().toString().trim().length()>2 &&
                descripcion.getText().toString().trim().length()> 0 ){
            return true;
        }
        Mensaje("Por favor completa todos los espacios", Toast.LENGTH_SHORT);
        return false;
    }

    public void Mensaje(String msg,int duracion){
        Toast.makeText(getApplicationContext(), msg, duracion).show();
    }


}

