package com.una.kinder.actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.firestore.FirebaseFirestore;

import com.google.firebase.iid.FirebaseInstanceId;
import com.una.kinder.R;
import com.una.kinder.entidades.Notificacion;
import com.una.kinder.entidades.Padre;
import com.una.kinder.entidades.Profesor;
import com.una.kinder.entidades.Usuario;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class ActividadRegistrarse extends AppCompatActivity {
    private String tipoUsuario;
    private RadioButton radioButtonPadre;
    private RadioButton radioButtonProfesor;
    private EditText cedula;
    private EditText nombre;
    private EditText apellido;
    private EditText numtel;
    private EditText correo;
    private EditText contrasena;
    private FirebaseAuth auth;
    private ProgressDialog progressDialogRegistrar;
    private FirebaseDatabase database;
    private DatabaseReference notificaciones;
    private DatabaseReference myRef;
    private FirebaseFirestore mFirestore;
    private String tempUsuarioToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_registrarse);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialogRegistrar = new ProgressDialog(this);
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        setupInputs();
    }// Fin del Oncreate ActividadRegistrarse

    private void setupInputs() {
        cedula = (EditText) findViewById(R.id.input_cedula);
        nombre = (EditText) findViewById(R.id.input_nombre);
        apellido = (EditText) findViewById(R.id.input_apellido);
        numtel = (EditText) findViewById(R.id.input_telefono);
        correo = (EditText) findViewById(R.id.input_email);
        contrasena = (EditText) findViewById(R.id.input_password);
        radioButtonProfesor = (RadioButton) findViewById(R.id.input_profesor);
        radioButtonPadre = (RadioButton) findViewById(R.id.input_padre);
        tipoUsuario = "";
        onclickRegistro(R.id.btn_registrarse);
        OnclickDelTipousuario(R.id.input_padre);
        OnclickDelTipousuario(R.id.input_profesor);
    }

    private void onclickRegistro(int ref) {
        View view = findViewById(ref);
        Button btnRegistro = (Button) view;
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniciarRegistro();
            }
        });
    } //fin del onclick del boton Registrase

    public void OnclickDelTipousuario(int ref) {
        View view = findViewById(ref);
        RadioButton miRadioButton = (RadioButton) view;
        miRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.input_padre:
                        tipoUsuario = FirebaseUtils.TIPO_USUARIO_PADRE;
                        break;
                    case R.id.input_profesor:
                        tipoUsuario = FirebaseUtils.TIPO_USUARIO_PROFESOR;
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void iniciarRegistro() {
        String pass = contrasena.getText().toString();
        String corre = correo.getText().toString();
        if (camposValidos()) {
            if (pass.length() > 5)
                correoYaExiste(corre, pass, tomarDatosUsuario());
            else
                Mensaje("La contraseña debe tener almenos 6 caracteres", 1);
        } else
            Mensaje("Completa todos los campos solicitados", 1);
    }

    public void registrarUsuario(final String correo, String contrasena, final Usuario usuario) {
        auth.createUserWithEmailAndPassword(correo, contrasena).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Usuario user=usuario;
                    user.setIndexUsuario(usuarioActual().getUid());
                    database.getReference(FirebaseUtils.CHILD_USUARIO).child(usuarioActual().getUid()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> taskRegistroUsuario) {

                            if (taskRegistroUsuario.isSuccessful()) {
                                enviarCorreoVerificacion(correo);
                                switch (usuario.getTipo()) {
                                    case FirebaseUtils.TIPO_USUARIO_PADRE:
                                        Padre padre = new Padre();
                                        padre.setIndexUsuario(usuarioActual().getUid());
                                        database.getReference(usuario.getTipo()).child(usuarioActual().getUid()).setValue(padre);
                                        break;
                                    case FirebaseUtils.TIPO_USUARIO_PROFESOR:
                                        Profesor profesor = new Profesor();
                                        profesor.setIndexUsuario(usuarioActual().getUid());
                                        database.getReference(usuario.getTipo()).child(usuarioActual().getUid()).setValue(profesor);
                                        break;
                                }
                                progressDialogRegistrar.dismiss();
                                Intent intent = new Intent(getApplicationContext(), ActividadLogin.class);
                                Mensaje("Confirma tu correo electronico y podras iniciar sesion", 1);
                                startActivity(intent);
                                finish();
                            } else {
                                progressDialogRegistrar.dismiss();
                                Mensaje("se ha presentado un error al registrar el usuario", 1);
                            }
                        }
                    });
                } else {
                    progressDialogRegistrar.hide();
                    finish();
                    Mensaje("Se ha presentado un error al registrar el usuario", 1);
                }
            }
        });
    }

    private void enviarCorreoVerificacion(String correo) {
        usuarioActual().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful())
                    Mensaje("Se ha presentado un error al enviar confirmación de correo", 1);
            }
        });
    }

    private boolean camposValidos() {
        if (numtel.getText().toString().trim().length() > 0 &&
                nombre.getText().toString().trim().length() > 0 &&
                apellido.getText().toString().trim().length() > 0 &&
                contrasena.getText().toString().trim().length() > 0 &&
                correo.getText().toString().trim().length() > 0 &&
                cedula.getText().toString().trim().length() > 0 &&
                tipoUsuario.length() > 0)
            return true;
        return false;
    }

    private Usuario tomarDatosUsuario() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy");
        String fecha= simpleDateFormat.format(date);
        Usuario usuario = new Usuario();
        usuario.setFechaCreacion(fecha);
        usuario.setTipo(tipoUsuario);
        usuario.setNombre(nombre.getText().toString());
        usuario.setApellidos(apellido.getText().toString());
        usuario.setToken(FirebaseUtils.obtenerToken());
        usuario.setNumeroTelefono(numtel.getText().toString());
        usuario.setIndexCedula(cedula.getText().toString());
        return usuario;
    }//fin tomar datos usuario

    private void correoYaExiste(final String corre, final String pass, final Usuario usuario) {
        mostrarProgressDialogRegistrar();
        auth.fetchProvidersForEmail(corre).addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                boolean existe = !task.getResult().getProviders().isEmpty();//dice si el correo ya existe
                if (existe == false)
                    registrarUsuario(corre, pass, usuario);
                else {
                    progressDialogRegistrar.hide();
                    Mensaje("El correo " + corre + " ya se encuentra registrado", 1);
                }
            }
        });
    }

    private void mostrarProgressDialogRegistrar() {
        progressDialogRegistrar.setTitle("Registrando Usuario");
        progressDialogRegistrar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialogRegistrar.setMessage("Espera por favor");
        progressDialogRegistrar.show();
    }

    private FirebaseUser usuarioActual() {
        return auth.getInstance().getCurrentUser();
    }

    public void Mensaje(String msg, int tiempo) {
        Toast.makeText(getApplicationContext(), msg, tiempo).show();
    }

} // [3:32:22 PM] Fin de la Clase ActividadRegistrarse








