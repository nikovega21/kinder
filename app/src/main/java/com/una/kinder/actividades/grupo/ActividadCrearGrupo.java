package com.una.kinder.actividades.grupo;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.entidades.Grupo;

public class ActividadCrearGrupo extends AppCompatActivity {

    private FirebaseDatabase database;
    private DatabaseReference ref;

    private EditText nombreText;
    private EditText numeroText;
    private EditText descripcionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_crear_grupo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        database = FirebaseDatabase.getInstance();
        ref = database.getReference(FirebaseUtils.CHILD_GRUPO);

        setupInputs();
    }

    private void setupInputs() {
        nombreText = findViewById(R.id.nombre_grupo);
        numeroText = findViewById(R.id.numero_grupo);
        descripcionText = findViewById(R.id.descripcion_grupo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_crear_grupo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.guardar_grupo:
                if (validarCampos()) {
                    Grupo grupo = new Grupo();
                    grupo.setNombre(nombreText.getText().toString());
                    grupo.setNumero(Integer.parseInt(numeroText.getText().toString()));
                    grupo.setDescripcion(descripcionText.getText().toString());
                    grupo.setIndexProfesor(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    registrarGrupo(grupo);
                } else {
                    Toast.makeText(this, "Debe de ingresar todos los campos", Toast.LENGTH_LONG).show();
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validarCampos() {
        return !(TextUtils.isEmpty(nombreText.getText().toString())) && TextUtils.isDigitsOnly(numeroText.getText().toString()) && !(TextUtils.isEmpty(descripcionText.getText().toString()));
    }

    private void registrarGrupo(Grupo grupo) {
        ref.push().setValue(grupo).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(ActividadCrearGrupo.this, "Grupo agregado con exito.", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(ActividadCrearGrupo.this, "El grupo no se pudo agregar.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
