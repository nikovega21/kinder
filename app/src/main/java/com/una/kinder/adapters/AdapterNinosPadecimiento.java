package com.una.kinder.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.una.kinder.R;
import com.una.kinder.actividades.ActividadListadoPadecimientos;
import com.una.kinder.entidades.Nino;

import java.util.ArrayList;

public class AdapterNinosPadecimiento extends RecyclerView.Adapter <AdapterNinosPadecimiento.NinosPadecimiento> {

    ArrayList<Nino> ninos;

    public AdapterNinosPadecimiento(ArrayList<Nino> ninos) {
        this.ninos = ninos;
    }

    @NonNull
    @Override
    public NinosPadecimiento onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nino_padecimiento, parent, false);
        return new NinosPadecimiento(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NinosPadecimiento holder, int position) {
        Nino n=ninos.get(position);
        holder.indexNino= n.getLlave();
        holder.nombre.setText(n.getNombre());
        if(n.getUrlImagen()!="default"){
            holder.image.setImageURI(n.getUrlImagen());
        }
    }

    @Override
    public int getItemCount() {
        return ninos.size();
    }

    class NinosPadecimiento extends RecyclerView.ViewHolder implements View.OnClickListener {

        String indexNino;
        TextView nombre;
        SimpleDraweeView image;

        public NinosPadecimiento(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            nombre = (TextView) itemView.findViewById(R.id.nino_nameP);
            image=(SimpleDraweeView) itemView.findViewById(R.id.nino_imagenP);
        }

        @Override
        public void onClick(View v) {
            Context c=v.getContext();
            Intent intento=new Intent (c, ActividadListadoPadecimientos.class);
            intento.putExtra("indexNino", indexNino);
            c.startActivity(intento);
        }

    }

}
