package com.una.kinder.adapters;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.una.kinder.R;
import com.una.kinder.actividades.ActividadModificarNino;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.entidades.Articulo;
import com.una.kinder.entidades.Nino;

import java.util.ArrayList;

public class AdapterArticulosNino extends RecyclerView.Adapter<AdapterArticulosNino.GrupoNinos> {
    ArrayList<Articulo> arrayArticulo;
    Context contexto;
    Nino ninoOpcionUndo;
    int indexOpcionUndo;

    public AdapterArticulosNino(ArrayList<Articulo> arrayNino, Context context) {
        this.arrayArticulo = arrayNino;
        contexto = context;
    }

    @Override
    public GrupoNinos onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_articulos, parent, false);
        return new GrupoNinos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GrupoNinos holder, final int position) {
        Articulo art = arrayArticulo.get(position);
        holder.nombre.setText(art.getNombre());
        holder.cantidad.setText(art.getCantidad());
        if(art.getUrlImagen()!="default"){
        holder.profile.setImageURI(art.getUrlImagen());
        }

    }

    @Override
    public int getItemCount() {
        return arrayArticulo.size();
    }

    public class GrupoNinos extends RecyclerView.ViewHolder {
        TextView nombre;
        SimpleDraweeView profile;
        TextView  cantidad;

        public GrupoNinos(View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.articulo_nombre);
            profile=(SimpleDraweeView) itemView.findViewById(R.id.imagen_articulo);
            cantidad=(TextView) itemView.findViewById(R.id.cantidad_articulo);
        }
    }


    public void remove(final int position) {

        indexOpcionUndo=position;
        FirebaseUtils.database.getReference(FirebaseUtils.CHILD_ARTICULO).child(arrayArticulo.get(position).getIndexNino()).child(arrayArticulo.get(position).getLlave()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    FirebaseUtils.database.getReference(FirebaseUtils.CHILD_PADRE).child(FirebaseAuth.getInstance().getUid()).child("ninos").child(arrayArticulo.get(position).getLlave()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(!task.isSuccessful())
                                Mensaje("Se ha presentado un inconveniente, intentalo mas tarde");
                            else{
                                arrayArticulo.remove(position);
                                notifyDataSetChanged();
                                Mensaje("Actualizado correctamente");
                            }
                        }
                    });

                }else
                    Mensaje("Se ha presentado un inconveniente, intentalo mas tarde");
            }
        });
    }
        public void Mensaje(String msj){
        Toast.makeText(contexto,msj,Toast.LENGTH_LONG).show();
    }
}
