package com.una.kinder.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.maps.MapView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.actividades.ActividadCrearPost;
import com.una.kinder.actividades.ActividadMapa;
import com.una.kinder.R;
import com.una.kinder.actividades.ActividadVisualizarImagen;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.entidades.Post;
import com.una.kinder.entidades.Usuario;

import java.util.ArrayList;
import java.util.HashMap;

import io.github.ponnamkarthik.richlinkpreview.MetaData;
import io.github.ponnamkarthik.richlinkpreview.ResponseListener;
import io.github.ponnamkarthik.richlinkpreview.RichLinkView;
import io.github.ponnamkarthik.richlinkpreview.RichLinkViewSkype;
import io.github.ponnamkarthik.richlinkpreview.RichPreview;
import io.github.ponnamkarthik.richlinkpreview.ViewListener;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostHolder> {
    private ArrayList<Post> posts;

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    private Context context;
    private MapView mapView;

    public PostAdapter(Context c, ArrayList<Post> posts) {
        this.posts = posts;
        context = c;
    }

    @NonNull
    @Override
    public PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_post, parent, false);
        return new PostHolder(view);
    }

    @Override
    public void onBindViewHolder(final PostHolder holder, int position) {
        holder.setIsRecyclable(false);
        //Obteniendo canciones segun posicion en arraylist
        final Post post = posts.get(position);
        holder.userNameView.setText("Nombre usuario");
        if (!TextUtils.isEmpty(post.getTexto())) {
            holder.textView.setText(post.getTexto());
        } else {
            holder.textView.setVisibility(View.GONE);
        }

        holder.dateView.setText(post.getFecha());
        FirebaseDatabase.getInstance().getReference(FirebaseUtils.CHILD_USUARIO).child(post.getIndexProfesor()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Usuario user = dataSnapshot.getValue(Usuario.class);
                holder.userNameView.setText((user.getNombre() + " " + user.getApellidos()));
                Uri uri = Uri.parse(user.getUrlImagen());
                holder.userImageView.setImageURI(uri);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        holder.contenidoPost.removeAllViews();
        if (post.getTipo().equals("imagen")) {
            final Uri uri = Uri.parse(post.getData().get("image_url"));
            SimpleDraweeView draweeView = new SimpleDraweeView(context);
            draweeView.setImageURI(uri);
            draweeView.setAdjustViewBounds(true);
            holder.contenidoPost.addView(draweeView);
            draweeView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ActividadVisualizarImagen.class);
                    intent.putExtra("imagen_url", uri.toString());
                    context.startActivity(intent);
                }
            });
        } else if (post.getTipo().equals("map")) {
            final String[] location = {post.getData().get("lat"), post.getData().get("lng")};
            final Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/staticmap?center=" + location[0] + "," + location[1] + "&format=JPEG&scale=2&zoom=18&size=500x500&markers=scale:2|" + location[0] + "," + location[1] + "&key=AIzaSyCo0jFiqud4O2rWrBc3WvOEtzVP12KptJM");
            SimpleDraweeView draweeView = new SimpleDraweeView(context);
            draweeView.setImageURI(uri);
            draweeView.setAdjustViewBounds(true);
            holder.contenidoPost.addView(draweeView);
            draweeView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ActividadMapa.class);
                    intent.putExtra("latlng", location[0] + "," + location[1]);
                    context.startActivity(intent);
                }
            });
            /*
            String[] location = post.getData().split(",");
            final LatLng latLng = new LatLng(Double.parseDouble(location[0]), Double.parseDouble(location[1]));
            GoogleMapOptions options = new GoogleMapOptions();
            options.liteMode(true);
            MapView mapView = new MapView(context, options);
            mapView.onCreate(null);
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, context.getResources().getDisplayMetrics());
            mapView.setLayoutParams(new LinearLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height));
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    MapsInitializer.initialize(context.getApplicationContext());
                    googleMap.addMarker(new MarkerOptions().position(latLng).title("Ubicación").flat(true));
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.builder().target(latLng).zoom(15).build()));
                }
            });
            holder.contenidoPost.addView(mapView);*/
        } else if (post.getTipo().equals("link")) {
            /*post.getData().get("descripcion");

            post.getData().get("url");
            post.getData().get("mediatype");*/
            View v = LayoutInflater.from(context).inflate(R.layout.item_rich_link, null);
            SimpleDraweeView imagen = v.findViewById(R.id.imagen_link);
            imagen.setImageURI(Uri.parse(post.getData().get("image_url")));
            TextView titulo = v.findViewById(R.id.titulo_link);
            titulo.setText((post.getData().get("sitio") + " | " + post.getData().get("titulo")));
            TextView descripcion = v.findViewById(R.id.descripcion_link);
            descripcion.setText(post.getData().get("descripcion"));
            holder.contenidoPost.addView(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = post.getData().get("url");
                    if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
                    Intent i = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                    context.startActivity(i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class PostHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView userImageView;
        TextView userNameView;
        TextView textView;
        TextView dateView;
        FrameLayout contenidoPost;

        public PostHolder(View itemView) {
            super(itemView);
            userImageView = itemView.findViewById(R.id.imagen_usuario_post);
            userNameView = itemView.findViewById(R.id.nombre_usuario_post);
            textView = itemView.findViewById(R.id.mensaje_post);
            dateView = itemView.findViewById(R.id.fecha_post);
            contenidoPost = itemView.findViewById(R.id.frame_contenido_post);
        }
    }

}
