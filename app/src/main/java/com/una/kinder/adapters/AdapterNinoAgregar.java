package com.una.kinder.adapters;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.una.kinder.R;
import com.una.kinder.actividades.grupo.ActividadInfoGrupo;
import com.una.kinder.entidades.Nino;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdapterNinoAgregar extends RecyclerView.Adapter<AdapterNinoAgregar.NinoHolder> {
    private ArrayList<Nino> ninos;
    private Map agregarNinos;

    public Map getAgregarNinos() {
        return agregarNinos;
    }

    public AdapterNinoAgregar(ArrayList<Nino> ninos) {
        this.ninos = ninos;
        agregarNinos = new HashMap<String, Boolean>();
    }

    @Override
    public NinoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nino_agregar, parent, false);
        return new NinoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NinoHolder holder, final int position) {
        final Nino n = ninos.get(position);
        holder.nombre.setText(n.getNombre() + " " + n.getApellidos());
        holder.check.setChecked(agregarNinos.containsKey(n.getLlave()));
        holder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.check.isChecked()) {
                    agregarNinos.remove(n.getLlave());
                } else {
                    agregarNinos.put(n.getLlave(), true);
                }
            }
        });
        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), ActividadInfoGrupo.class);
                intent.putExtra("id", 123);
                holder.itemView.getContext().startActivity(intent);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return ninos.size();
    }

    public void remove(int position) {
        ninos.remove(position);
        notifyItemRemoved(position);
    }

    public ArrayList<Nino> getNinos() {
        return ninos;
    }

    public Nino get(int position) {
        return ninos.get(position);
    }

    public void agregar(Nino nino) {
        ninos.add(nino);
        notifyItemInserted(ninos.size());
    }

    public class NinoHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        CheckBox check;

        public NinoHolder(View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.nino_name);
            check = itemView.findViewById(R.id.check_nino_existe);
        }
    }
}
