package com.una.kinder.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.una.kinder.R;
import com.una.kinder.actividades.ActividadModificarPadecimiento;
import com.una.kinder.entidades.Padecimiento;

import java.util.ArrayList;

public class AdapterPadecimiento extends RecyclerView.Adapter <AdapterPadecimiento.PadecimientoViewHolder>  {

    ArrayList<Padecimiento> padecimientos;

    public AdapterPadecimiento(ArrayList<Padecimiento> padecimientos) {
        this.padecimientos = padecimientos;
    }
    @NonNull
    @Override
    public PadecimientoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_info_padecimiento, parent, false);
        return new PadecimientoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PadecimientoViewHolder holder, int position) {
        Padecimiento p=padecimientos.get(position);
        holder.indexPadecimiento=p.getIndexPadecimiento();
        holder.titulo.setText("Nombre: "+p.getTipo());
        holder.descripcion.setText("Descripción: "+p.getDescripcion());
    }

    @Override
    public int getItemCount() {
        return padecimientos.size();
    }

    public class PadecimientoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        String indexPadecimiento;
        TextView titulo;
        TextView descripcion;

        public PadecimientoViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            titulo = (TextView) itemView.findViewById(R.id.name_pad);
            descripcion=(TextView) itemView.findViewById(R.id.descripcion_pad);

        }

        @Override
        public void onClick(View v) {
            Context c=v.getContext();
            Intent intento=new Intent (c, ActividadModificarPadecimiento.class);
            intento.putExtra("indexPadecimiento", indexPadecimiento);
            intento.putExtra("tipo", titulo.getText());
            intento.putExtra("descripcion", descripcion.getText());
            c.startActivity(intento);


        }

    }
}


