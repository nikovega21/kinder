package com.una.kinder.adapters;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.una.kinder.R;
import com.una.kinder.actividades.grupo.ActividadCrearGrupo;
import com.una.kinder.actividades.grupo.ActividadInfoGrupo;
import com.una.kinder.entidades.Grupo;

import java.util.ArrayList;

public class AdapterGrupo extends RecyclerView.Adapter<AdapterGrupo.GrupoHolder> {
    private ArrayList<Grupo> grupos;

    public AdapterGrupo(ArrayList<Grupo> grupos) {
        this.grupos = grupos;
    }

    @Override
    public GrupoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group, parent, false);
        return new GrupoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GrupoHolder holder, int position) {
        final Grupo n = grupos.get(position);
        holder.nombre.setText(n.getNombre());
        holder.cantidad.setText("Cantidad: 20");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), ActividadInfoGrupo.class);
                intent.putExtra("id", n.getIndexGrupo());
                intent.putExtra("nombre", n.getNombre());
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    public void agregar(Grupo grupo) {
        grupos.add(grupo);
        notifyItemInserted(grupos.size());
    }

    public void remove(int position) {
        grupos.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return grupos.size();
    }

    public class GrupoHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        TextView cantidad;

        public GrupoHolder(View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.group_name);
            cantidad = (TextView) itemView.findViewById(R.id.group_size);

        }
    }
}
