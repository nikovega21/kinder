package com.una.kinder.adapters;


import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.una.kinder.R;
import com.una.kinder.actividades.grupo.ActividadInfoGrupo;
import com.una.kinder.actividades.grupo.ActividadInfoPadre;
import com.una.kinder.entidades.Nino;
import com.una.kinder.entidades.Usuario;

import java.util.ArrayList;

public class AdapterPadreGrupo extends RecyclerView.Adapter<AdapterPadreGrupo.PadreHolder> {
    ArrayList<Usuario> padres;

    public ArrayList<Usuario> getPadres() {
        return padres;
    }

    public void setPadres(ArrayList<Usuario> padres) {
        this.padres = padres;
    }

    public AdapterPadreGrupo(ArrayList<Usuario> padres) {
        this.padres = padres;
    }

    @Override
    public PadreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_padre_grupo, parent, false);
        return new PadreHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PadreHolder holder, int position) {
        final Usuario n = padres.get(position);
        holder.nombre.setText(n.getNombre() + " " + n.getApellidos());
        holder.image.setImageURI(Uri.parse(n.getUrlImagen()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), ActividadInfoPadre.class);
                intent.putExtra("id", n.getIndexUsuario());
                holder.itemView.getContext().startActivity(intent);
            }
        });

    }

    public void remove(int position) {
        padres.remove(position);
        notifyItemRemoved(position);
    }

    public Usuario get(int position) {
        return padres.get(position);
    }

    public void agregar(Usuario usuario) {
        padres.add(usuario);
        notifyItemInserted(padres.size());
    }

    @Override
    public int getItemCount() {
        return padres.size();
    }

    public class PadreHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        SimpleDraweeView image;

        public PadreHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imagen_padre);
            nombre = (TextView) itemView.findViewById(R.id.nino_name);
        }
    }
}
