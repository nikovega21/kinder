package com.una.kinder.adapters;


import android.content.Context;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.una.kinder.R;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.entidades.Notificacion;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdapterNotificaciones extends RecyclerView.Adapter<AdapterNotificaciones.GrupoNotificaciones> {
    ArrayList<Notificacion> notificaciones;
    Context contexto;
    Map<Integer, Integer> paletaColores ;
    Map<Integer, Integer> imagenes ;

    public AdapterNotificaciones(ArrayList<Notificacion> notificaciones, Context context) {
        this.notificaciones = notificaciones;
        contexto = context;
        paletaColores= new HashMap();
        imagenes=new HashMap<>();
        setPaletaColores();
    }

    @Override
    public GrupoNotificaciones onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_notificacion, parent, false);
        return new GrupoNotificaciones(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GrupoNotificaciones holder, final int position) {

        Notificacion n = notificaciones.get(position);
        holder.titulo.setText(n.getAsunto());
        holder.descripcion.setText(n.getMensaje());
        holder.fecha.setText(n.getFecha());
        String colorHex = "#" + Integer.toHexString(ContextCompat.getColor(contexto,paletaColores.get(n.getImageCode())) & 0x00ffffff);
        holder.icono.setImageDrawable(setTint(contexto.getResources().getDrawable(imagenes.get(n.getImageCode())), colorHex));
    }

    public void remove(final int position) {
        FirebaseUtils.database.getReference(FirebaseUtils.CHILD_NOTIFICACION).child(FirebaseUtils.auth.getCurrentUser().getUid())
                .child(notificaciones.get(position).getKey()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        notificaciones.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(contexto, "Eliminado correctamente", Toast.LENGTH_SHORT).show();
                    }    else
                    {
                        Toast.makeText(contexto, "Se ha presentado un inconveniente intentalo mas tarde", Toast.LENGTH_SHORT).show();
                    }
            }
        });

    }
    public static Drawable setTint(Drawable d, String color) {
        ColorFilter filter = new LightingColorFilter(Color.parseColor(color), Color.parseColor(color));
        d.setColorFilter(filter);
        return d;
    }

    @Override
    public int getItemCount() {
        return notificaciones.size();
    }

    public class GrupoNotificaciones extends RecyclerView.ViewHolder {
        TextView titulo;
        TextView descripcion;
        TextView fecha;
        ImageView icono;


        public GrupoNotificaciones(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.titulo);
            descripcion = (TextView) itemView.findViewById(R.id.descripcion);
            fecha = (TextView) itemView.findViewById(R.id.fecha);
            icono = (ImageView) itemView.findViewById(R.id.icono_notificacion);
        }
    }

    public void setPaletaColores() {

        paletaColores.put(0, R.color.colorVerde);
        paletaColores.put(1,R.color.colorCelesteClaro);
        imagenes.put(0,R.drawable.ic_account_circle_black_24dp);
        imagenes.put(1,R.drawable.ic_agregado_grupo);
    }
}
