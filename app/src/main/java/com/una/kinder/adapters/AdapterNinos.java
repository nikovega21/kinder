package com.una.kinder.adapters;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.una.kinder.R;
import com.una.kinder.actividades.ActividadModificarNino;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.actividades.articulos.ActividadMantenimientoArticulos;
import com.una.kinder.entidades.Nino;

import java.util.ArrayList;

public class AdapterNinos extends RecyclerView.Adapter<AdapterNinos.GrupoNinos> {
    ArrayList<Nino> arrayNino;
    Context contexto;
    Nino ninoOpcionUndo;
    int indexOpcionUndo;

    public AdapterNinos(ArrayList<Nino> arrayNino, Context context) {
        this.arrayNino = arrayNino;
        contexto = context;
    }

    @Override
    public GrupoNinos onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_ninos, parent, false);
        return new GrupoNinos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GrupoNinos holder, final int position) {
        Nino n = arrayNino.get(position);
        holder.nombre.setText(n.getNombre());

        if(n.getUrlImagen()!="default"){
        holder.profile.setImageURI(n.getUrlImagen());
        }

        holder.nombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Nino n = arrayNino.get(position);
                Intent i = new Intent(contexto, ActividadModificarNino.class);
                i.putExtra("id_nino", n.getLlave());
                contexto.startActivity(i);
            }
        });

        holder.img_articulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Nino n = arrayNino.get(position);
                Intent i = new Intent(contexto, ActividadMantenimientoArticulos.class);
                i.putExtra("id_nino", n.getLlave());
                contexto.startActivity(i);
            }
        });

    }

    public static Drawable setTint(Drawable d, String color) {
        ColorFilter filter = new LightingColorFilter(Color.parseColor(color), Color.parseColor(color));
        d.setColorFilter(filter);
        return d;
    }

    @Override
    public int getItemCount() {
        return arrayNino.size();
    }

    public class GrupoNinos extends RecyclerView.ViewHolder {
        TextView nombre;
        SimpleDraweeView profile;
        ImageView img_articulo;

        public GrupoNinos(View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.nino_nombre);
            profile=(SimpleDraweeView) itemView.findViewById(R.id.nino_img_adapter_nino);
            img_articulo=(ImageView) itemView.findViewById(R.id.add_articulo);

        }
    }

    public void remove(final int position) {
        ninoOpcionUndo=arrayNino.get(position);
        indexOpcionUndo=position;
        FirebaseUtils.database.getReference(FirebaseUtils.CHILD_NINO).child(arrayNino.get(position).getLlave()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){

                        FirebaseUtils.database.getReference(FirebaseUtils.CHILD_PADRE).child(FirebaseAuth.getInstance().getUid()).child("ninos").child(arrayNino.get(position).getLlave()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(!task.isSuccessful())
                                Mensaje("Se ha presentado un inconveniente, intentalo mas tarde");
                            else{
                                arrayNino.remove(position);
                                notifyDataSetChanged();
                                Mensaje("Actualizado correctamente");
                            }
                        }
                    });

                }else
                    Mensaje("Se ha presentado un inconveniente, intentalo mas tarde");
            }
        });
    }

    public void Mensaje(String msj){
        Toast.makeText(contexto,msj,Toast.LENGTH_LONG).show();
    }
}
