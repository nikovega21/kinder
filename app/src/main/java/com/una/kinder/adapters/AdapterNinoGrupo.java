package com.una.kinder.adapters;


import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.una.kinder.R;
import com.una.kinder.actividades.grupo.ActividadInfoGrupo;
import com.una.kinder.actividades.grupo.ActividadInfoNino;
import com.una.kinder.entidades.Grupo;
import com.una.kinder.entidades.Nino;

import java.util.ArrayList;

public class AdapterNinoGrupo extends RecyclerView.Adapter<AdapterNinoGrupo.NinoHolder> {
    ArrayList<Nino> ninos;

    public ArrayList<Nino> getNinos() {
        return ninos;
    }

    public AdapterNinoGrupo(ArrayList<Nino> ninos) {
        this.ninos = ninos;
    }

    @NonNull
    @Override
    public NinoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nino_grupo, parent, false);
        return new NinoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NinoHolder holder, int position) {
        final Nino n = ninos.get(position);
        holder.nombre.setText(n.getNombre() + " " + n.getApellidos());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), ActividadInfoNino.class);
                intent.putExtra("id_nino", n.getLlave());
                holder.itemView.getContext().startActivity(intent);
            }
        });
        holder.image.setImageURI(Uri.parse(n.getUrlImagen()));
    }

    @Override
    public int getItemCount() {
        return ninos.size();
    }

    public void remove(int position) {
        ninos.remove(position);
        notifyItemRemoved(position);
    }

    public Nino get(int position) {
        return ninos.get(position);
    }

    public void agregar(Nino nino) {
        ninos.add(nino);
        notifyItemInserted(ninos.size());
    }

    class NinoHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        SimpleDraweeView image;
        NinoHolder(View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.nino_name);
            image = itemView.findViewById(R.id.image_nino_grupo);
        }
    }
}
