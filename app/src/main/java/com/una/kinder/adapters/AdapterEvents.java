package com.una.kinder.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.una.kinder.actividades.ActividadVerEvento;
import com.una.kinder.R;
import com.una.kinder.entidades.Evento;

import java.util.ArrayList;

public class AdapterEvents extends RecyclerView.Adapter<AdapterEvents.GrupoEventos> {
    ArrayList<Evento> eventos;

    public AdapterEvents(ArrayList<Evento> eventos) {
        this.eventos = eventos;
    }



    @Override
    public GrupoEventos onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_evento, parent, false);
        return new GrupoEventos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GrupoEventos holder, int position) {
        Evento n=eventos.get(position);
        holder.indexEvento=n.getIndexEvento();
        holder.fecha.setText(n.getFecha());
        holder.descripcion.setText(n.getDescripción());
        holder.lugar.setText(n.getLugar());
        holder.hora.setText(n.getHora());
        holder.indexGrupo.setText(n.getIndexGrupo());
    }

    @Override
    public int getItemCount() {
        return eventos.size();
    }

    public class GrupoEventos extends RecyclerView.ViewHolder implements View.OnClickListener {
        String indexEvento;
        TextView descripcion;
        TextView fecha;
        TextView lugar;
        TextView hora;
        TextView indexGrupo;

        public GrupoEventos(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
           descripcion=(TextView) itemView.findViewById(R.id.descripcion) ;
            fecha = (TextView) itemView.findViewById(R.id.fecha);
            lugar = (TextView) itemView.findViewById(R.id.lugar);
            hora = (TextView) itemView.findViewById(R.id.hora);
            indexGrupo = (TextView) itemView.findViewById(R.id.indexGrupo);
        }

        @Override
        public void onClick(View v){
            Context c=v.getContext();
            Intent intento=new Intent (c, ActividadVerEvento.class);
            intento.putExtra("actividad", indexEvento);
            intento.putExtra("descripcion", descripcion.getText().toString());
            intento.putExtra("fecha", fecha.getText().toString());
            intento.putExtra("lugar", lugar.getText().toString());
            intento.putExtra("hora", hora.getText().toString());
            intento.putExtra("grupo", indexGrupo.getText().toString());
            c.startActivity(intento);
        }
    }
}
