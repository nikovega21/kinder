package com.una.kinder.entidades;

import java.util.HashMap;

/**
 * Created by nicoa on 25/3/2018.
 */

public class Nino {
    private String nombre;
    private String apellidos;
    private String llave;
    private String urlImagen;
    private int edad;
    private String indexGrupo;
    private String indexPadre;
    private String fechaNacimiento;
    private String telefonoEmergencia;
    private String indexNumeroCarnet;


    private HashMap<String, Boolean> grupos;

    /*
        private HashMap<String, Boolean> articulos;
        private HashMap<String, Boolean> medicinas;
        private HashMap<String, Boolean> padecimientos;
        private HashMap<String, Boolean> observaciones;
    */
    public HashMap<String, Boolean> getGrupos() {
        return grupos;
    }

    public void setGrupos(HashMap<String, Boolean> grupos) {
        this.grupos = grupos;
    }

    public Nino() {
        this.nombre = "default";
        this.apellidos = "default";
        this.indexPadre = "default";
        this.fechaNacimiento = "default";
        this.telefonoEmergencia = "default";
        this.indexNumeroCarnet = "default";
        this.urlImagen = "default";
        this.grupos = new HashMap<>();
    }

    public Nino(String nombre, String apellidos, String indexPadre, String fechaNacimiento, String telefonoEmergencia, String indexNumeroCarnet, int edad, String llave) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.indexPadre = indexPadre;
        this.fechaNacimiento = fechaNacimiento;
        this.telefonoEmergencia = telefonoEmergencia;
        this.indexNumeroCarnet = indexNumeroCarnet;
        this.edad = edad;
        this.llave = llave;
    }

    public Nino(String nombre, String apellidos, String indexPadre, String fechaNacimiento, String telefonoEmergencia, String indexNumeroCarnet, int edad, String llave, String urlImagen) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.indexPadre = indexPadre;
        this.fechaNacimiento = fechaNacimiento;
        this.telefonoEmergencia = telefonoEmergencia;
        this.indexNumeroCarnet = indexNumeroCarnet;
        this.edad = edad;
        this.llave = llave;
        this.urlImagen = urlImagen;
    }

    public Nino(String nombre, String apellidos, String indexGrupo, String indexPadre, String fechaNacimiento, String telefonoEmergencia, String indexNumeroCarnet, HashMap<String, Boolean> articulos, HashMap<String, Boolean> medicinas, HashMap<String, Boolean> padecimientos, HashMap<String, Boolean> observaciones) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.indexGrupo = indexGrupo;
        this.indexPadre = indexPadre;
        this.fechaNacimiento = fechaNacimiento;
        this.telefonoEmergencia = telefonoEmergencia;
        this.indexNumeroCarnet = indexNumeroCarnet;
        /*
        this.articulos = articulos;
        this.medicinas = medicinas;
        this.padecimientos = padecimientos;
        this.observaciones = observaciones;*/
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getIndexGrupo() {
        return indexGrupo;
    }

    public void setIndexGrupo(String indexGrupo) {
        this.indexGrupo = indexGrupo;
    }

    public String getIndexPadre() {
        return indexPadre;
    }

    public void setIndexPadre(String indexPadre) {
        this.indexPadre = indexPadre;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTelefonoEmergencia() {
        return telefonoEmergencia;
    }

    public void setTelefonoEmergencia(String telefonoEmergencia) {
        this.telefonoEmergencia = telefonoEmergencia;
    }

    public String getIndexNumeroCarnet() {
        return indexNumeroCarnet;
    }

    public void setIndexNumeroCarnet(String indexNumeroCarnet) {
        this.indexNumeroCarnet = indexNumeroCarnet;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImage) {
        this.urlImagen = urlImage;
    }

/*
    public HashMap<String, Boolean> getArticulos() {
        return articulos;
    }

    public void setArticulos(HashMap<String, Boolean> articulos) {
        this.articulos = articulos;
    }

    public HashMap<String, Boolean> getMedicinas() {
        return medicinas;
    }

    public void setMedicinas(HashMap<String, Boolean> medicinas) {
        this.medicinas = medicinas;
    }

    public HashMap<String, Boolean> getPadecimientos() {
        return padecimientos;
    }

    public void setPadecimientos(HashMap<String, Boolean> padecimientos) {
        this.padecimientos = padecimientos;
    }

    public HashMap<String, Boolean> getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(HashMap<String, Boolean> observaciones) {
        this.observaciones = observaciones;
    }
*/
}
