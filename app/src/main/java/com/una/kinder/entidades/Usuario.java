package com.una.kinder.entidades;

import java.util.HashMap;

/**
 * Created by geekMQ on 3/25/2018.
 */

public class Usuario {
    private String correo;
    private String indexCedula;//padre profe
    private String indexUsuario;
    private String tipo;//padre profe
    private String indexTipo;
    private String numeroTelefono;
    private String fechaCreacion;
    private String nombre;
    private String apellidos;
    private String urlImagen;
    private String token;

    HashMap<String, Boolean> notificaciones;

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    HashMap<String, Boolean> mensajes;
    HashMap<String, Boolean> grupos;

    public Usuario() {
        urlImagen = "default";
    }

    public Usuario(String correo, String indexCedula, String tipo, String numeroTelefono, String nombre, String apellidos, HashMap<String, Boolean> notificaciones, HashMap<String, Boolean> mensajes, HashMap<String, Boolean> grupos) {
        this.correo = correo;
        this.indexCedula = indexCedula;
        this.tipo = tipo;
        this.numeroTelefono = numeroTelefono;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.notificaciones = notificaciones;
        this.mensajes = mensajes;
        this.grupos = grupos;
    }

    public Usuario(String correo, String indexCedula, String tipo, String numeroTelefono, String nombre, String apellidos) {
        this.correo = correo;
        this.indexCedula = indexCedula;
        this.tipo = tipo;
        this.numeroTelefono = numeroTelefono;
        this.nombre = nombre;
        this.apellidos = apellidos;
    }


    public String getIndexUsuario() {
        return indexUsuario;
    }

    public void setIndexUsuario(String indexUsuario) {
        this.indexUsuario = indexUsuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getIndexCedula() {
        return indexCedula;
    }

    public void setIndexCedula(String indexCedula) {
        this.indexCedula = indexCedula;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public HashMap<String, Boolean> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(HashMap<String, Boolean> notificaciones) {
        this.notificaciones = notificaciones;
    }

    public HashMap<String, Boolean> getMensajes() {
        return mensajes;
    }

    public void setMensajes(HashMap<String, Boolean> mensajes) {
        this.mensajes = mensajes;
    }

    public HashMap<String, Boolean> getGrupos() {
        return grupos;
    }

    public void setGrupos(HashMap<String, Boolean> grupos) {
        this.grupos = grupos;
    }

    public String getIndexTipo() {
        return indexTipo;
    }

    public void setIndexTipo(String indexTipo) {
        this.indexTipo = indexTipo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
}
