package com.una.kinder.entidades;

import java.util.HashMap;

/**
 * Created by geekMQ on 3/25/2018.
 */

public class Padre {
    String indexUsuario;
    HashMap<String,Boolean> ninos;
    String domicilio;

    public Padre() {
    }
    public Padre(String indexUsuario, HashMap<String, Boolean> ninos, String domicilio) {
        this.indexUsuario = indexUsuario;
        this.ninos = ninos;
        this.domicilio = domicilio;
    }

    public String getIndexUsuario() {
        return indexUsuario;
    }

    public void setIndexUsuario(String indexUsuario) {
        this.indexUsuario = indexUsuario;
    }

    public HashMap<String, Boolean> getNinos() {
        return ninos;
    }

    public void setNinos(HashMap<String, Boolean> ninos) {
        this.ninos = ninos;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
}
