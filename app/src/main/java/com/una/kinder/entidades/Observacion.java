package com.una.kinder.entidades;

/**
 * Created by nicoa on 25/3/2018.
 */

public class Observacion {
    private String indexObservacion;
    private String texto;
    private String indexProfesor;
    private String indexNino;

    public Observacion() {
    }

    public Observacion(String indexObservacion, String texto, String indexProfesor, String indexNino) {
        this.indexObservacion = indexObservacion;
        this.texto = texto;
        this.indexProfesor = indexProfesor;
        this.indexNino = indexNino;
    }

    public String getIndexObservacion() {
        return indexObservacion;
    }

    public void setIndexObservacion(String indexObservacion) {
        this.indexObservacion = indexObservacion;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getIndexProfesor() {
        return indexProfesor;
    }

    public void setIndexProfesor(String indexProfesor) {
        this.indexProfesor = indexProfesor;
    }

    public String getIndexNino() {
        return indexNino;
    }

    public void setIndexNino(String indexNino) {
        this.indexNino = indexNino;
    }
}
