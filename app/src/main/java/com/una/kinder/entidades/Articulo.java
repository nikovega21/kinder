package com.una.kinder.entidades;

/**
 * Created by nicoa on 25/3/2018.
 */

public class Articulo {
    private String indexNino;
    private String nombre;
    private String cantidad;
    private String llave;
    private String urlImagen;

    public Articulo(String indexNino, String nombre, String cantidad, String indexArticulo) {
        this.indexNino = indexNino;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.llave = indexArticulo;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public Articulo() {
          indexNino="default" ;
          nombre="default" ;
          cantidad ="default";
        llave ="default";
          urlImagen="default" ;
    }

    public String getIndexNino() {
        return indexNino;
    }

    public void setIndexNino(String indexNino) {
        this.indexNino = indexNino;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String indexArticulo) {
        this.llave = indexArticulo;
    }
}
