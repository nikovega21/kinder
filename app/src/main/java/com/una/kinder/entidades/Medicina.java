package com.una.kinder.entidades;

import java.util.HashMap;

/**
 * Created by nicoa on 25/3/2018.
 */

public class Medicina {
    private String nombre;
    private String rangoTiempo;
    private String indexNino;
    private String indexMedicina;
    private HashMap<String,String> horarioPerzonalizado;

    public Medicina() {
    }

    public Medicina(String nombre, String rangoTiempo, String indexNino, String indexMedicina, HashMap<String, String> horarioPerzonalizado) {
        this.nombre = nombre;
        this.rangoTiempo = rangoTiempo;
        this.indexNino = indexNino;
        this.indexMedicina = indexMedicina;
        this.horarioPerzonalizado = horarioPerzonalizado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRangoTiempo() {
        return rangoTiempo;
    }

    public void setRangoTiempo(String rangoTiempo) {
        this.rangoTiempo = rangoTiempo;
    }

    public String getIndexNino() {
        return indexNino;
    }

    public void setIndexNino(String indexNino) {
        this.indexNino = indexNino;
    }

    public String getIndexMedicina() {
        return indexMedicina;
    }

    public void setIndexMedicina(String indexMedicina) {
        this.indexMedicina = indexMedicina;
    }

    public HashMap<String, String> getHorarioPerzonalizado() {
        return horarioPerzonalizado;
    }

    public void setHorarioPerzonalizado(HashMap<String, String> horarioPerzonalizado) {
        this.horarioPerzonalizado = horarioPerzonalizado;
    }
}
