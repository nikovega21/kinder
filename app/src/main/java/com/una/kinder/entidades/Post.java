package com.una.kinder.entidades;


import java.util.Map;

/**
 * Created by nicoa on 25/3/2018.
 */

public class Post {
    private String indexPublicacion;
    private String tipo;
    private String fecha;
    private String indexProfesor;
    private String texto;
    private String indexGrupo;
    private Map<String, String> data;


    public Post(String indexPublicacion, String tipo, String fecha, String indexProfesor, String texto, String indexGrupo, Map<String, String> data) {
        this.indexPublicacion = indexPublicacion;
        this.tipo = tipo;
        this.fecha = fecha;
        this.indexProfesor = indexProfesor;
        this.texto = texto;
        this.indexGrupo = indexGrupo;
        this.data = data;
    }

    public Post() {
    }

    public String getIndexPublicacion() {
        return indexPublicacion;
    }

    public void setIndexPublicacion(String indexPublicacion) {
        this.indexPublicacion = indexPublicacion;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIndexProfesor() {
        return indexProfesor;
    }

    public void setIndexProfesor(String indexProfesor) {
        this.indexProfesor = indexProfesor;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getIndexGrupo() {
        return indexGrupo;
    }

    public void setIndexGrupo(String indexGrupo) {
        this.indexGrupo = indexGrupo;
    }
}
