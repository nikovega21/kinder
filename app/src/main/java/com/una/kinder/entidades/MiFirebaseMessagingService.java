package com.una.kinder.entidades;


import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.una.kinder.R;
import com.una.kinder.actividades.ActividadLogin;
import com.una.kinder.actividades.ActividadPrincipal;


import java.util.Map;

public class MiFirebaseMessagingService extends FirebaseMessagingService {
    private FirebaseAuth auth;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> payload = remoteMessage.getData();
            Log.d("", "Message data payload: " + remoteMessage.getData());
            verNotoficacion(payload);

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void verNotoficacion(Map<String, String> payload) {
        auth=FirebaseAuth.getInstance();
        Intent intent;
        if(auth.getCurrentUser()!=null) {
            // Create an explicit intent for an Activity in your app
            intent = new Intent(this, ActividadPrincipal.class);
        }else{
            intent = new Intent(this, ActividadLogin.class);
        }

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(payload.get("nombreUsuario"))
                    .setContentText(payload.get("mensaje"))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            //este id hay que corregirlo

            notificationManager.notify(0, mBuilder.build());



    }
}
