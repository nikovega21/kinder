package com.una.kinder.entidades;

public class Notificacion {
    private String asunto;
    private String mensaje;
    private String fecha;
    private String indexUsuario;
    private String nombreUsuario;
    private String key;
    private int imageCode;
    private int tipo;
    private String token;


    public Notificacion() {

        this.mensaje = "default";
          asunto ="default";
          mensaje ="default";
          fecha="default" ;
          indexUsuario ="default";
          nombreUsuario="default" ;
          key="default";
          imageCode=0;
    }

    public Notificacion(String asunto, String mensaje, String fecha) {
        this.asunto = asunto;
        this.mensaje = mensaje;
        this.fecha = fecha;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIndexUsuario() {
        return indexUsuario;
    }

    public void setIndexUsuario(String indexUsuario) {
        this.indexUsuario = indexUsuario;
    }

    public int getImageCode() {
        return imageCode;
    }

    public void setImageCode(int imageCode) {
        this.imageCode = imageCode;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
