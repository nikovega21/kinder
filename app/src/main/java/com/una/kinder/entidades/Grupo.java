package com.una.kinder.entidades;

import java.util.HashMap;

/**
 * Created by nicoa on 25/3/2018.
 */

public class Grupo {

    private String indexGrupo;

    private String nombre;
    private String indexProfesor;
    private HashMap<String, Boolean> ninos;
    private String descripcion;
    private HashMap<String, Boolean> eventos;
    private int numero;

    public Grupo(String indexGrupo, String indexProfesor, HashMap<String, Boolean> ninos, String descripcion, HashMap<String, Boolean> eventos, int numero) {
        this.indexGrupo = indexGrupo;
        this.indexProfesor = indexProfesor;
        this.ninos = ninos;
        this.descripcion = descripcion;
        this.eventos = eventos;
        this.numero = numero;
    }

    public String getIndexGrupo() {
        return indexGrupo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setIndexGrupo(String indexGrupo) {
        this.indexGrupo = indexGrupo;
    }

    public HashMap<String, Boolean> getEventos() {
        return eventos;
    }

    public void setEventos(HashMap<String, Boolean> eventos) {
        this.eventos = eventos;
    }

    public Grupo() {

    }


    public String getIndexProfesor() {
        return indexProfesor;
    }

    public void setIndexProfesor(String indexProfesor) {
        this.indexProfesor = indexProfesor;
    }

    public HashMap<String, Boolean> getNinos() {
        return ninos;
    }

    public void setNinos(HashMap<String, Boolean> ninos) {
        this.ninos = ninos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }


}
