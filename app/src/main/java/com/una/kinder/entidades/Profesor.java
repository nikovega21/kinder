package com.una.kinder.entidades;

/**
 * Created by geekMQ on 3/25/2018.
 */

public class Profesor {
    String indexUsuario;
    String indexProfesor;
    public Profesor() {
    }

    public Profesor(String indexUsuario, String indexProfesor) {
        this.indexUsuario = indexUsuario;
        this.indexProfesor = indexProfesor;
    }


    public String getIndexUsuario() {
        return indexUsuario;
    }

    public void setIndexUsuario(String indexUsuario) {
        this.indexUsuario = indexUsuario;
    }

    public String getIndexProfesor() {
        return indexProfesor;
    }

    public void setIndexProfesor(String indexProfesor) {
        this.indexProfesor = indexProfesor;
    }
}

