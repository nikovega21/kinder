package com.una.kinder.entidades;

/**
 * Created by nicoa on 25/3/2018.
 */

public class Padecimiento {
    private String tipo;
    private String descripcion;
    private String indexNino;


    private String indexPadecimiento;

    public Padecimiento() {
    }

    public Padecimiento(String tipo, String descripcion, String indexNino, String indexPadecimiento) {
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.indexNino = indexNino;
        this.indexPadecimiento= indexPadecimiento;
    }
    public String getIndexPadecimiento() {
        return indexPadecimiento;
    }

    public void setIndexPadecimiento(String indexPadecimiento) {
        this.indexPadecimiento = indexPadecimiento;
    }
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIndexNino() {
        return indexNino;
    }

    public void setIndexNino(String indexNino) {
        this.indexNino = indexNino;
    }
}
