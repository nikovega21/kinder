package com.una.kinder.entidades;

/**
 * Created by nicoa on 25/3/2018.
 */

public class Evento {
    private String indexEvento;


    private String descripción;
    private String fecha;
    private String lugar;
    private String hora;
    private String indexGrupo;


    public Evento() {
    }

    public Evento(String indexEvento, String descripción, String fecha, String lugar, String hora, String indexGrupo) {
        this.indexEvento = indexEvento;
        this.descripción=descripción;
        this.fecha = fecha;
        this.lugar = lugar;
        this.hora = hora;
        this.indexGrupo = indexGrupo;
    }

    public String getDescripción() {
        return descripción;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    public String getIndexEvento() {
        return indexEvento;
    }

    public void setIndexEvento(String indexEvento) {
        this.indexEvento = indexEvento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getIndexGrupo() {
        return indexGrupo;
    }

    public void setIndexGrupo(String indexGrupo) {
        this.indexGrupo = indexGrupo;
    }
}
