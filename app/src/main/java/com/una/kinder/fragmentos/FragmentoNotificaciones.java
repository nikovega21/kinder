package com.una.kinder.fragmentos;

import android.app.Fragment;
import android.app.Notification;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.una.kinder.R;
import com.google.firebase.database.DatabaseReference;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.actividades.articulos.ActividadMantenimientoArticulos;
import com.una.kinder.adapters.AdapterArticulosNino;
import com.una.kinder.adapters.AdapterNotificaciones;
import com.una.kinder.entidades.Notificacion;
import com.una.kinder.entidades.Post;

import java.util.ArrayList;


public class FragmentoNotificaciones extends Fragment {
    private View view;
    private FirebaseAuth auth;
    private ArrayList<Notificacion> lista;
    private AdapterNotificaciones adapter;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;
    private RecyclerView recycler;

    public FragmentoNotificaciones() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        auth = FirebaseAuth.getInstance();
        view = inflater.inflate(R.layout.fragmento_notificaciones, container, false);
        database=FirebaseDatabase.getInstance();
        if(auth.getCurrentUser()!=null) {
            databaseReference = database.getReference(FirebaseUtils.CHILD_NOTIFICACION).child(auth.getCurrentUser().getUid());
            recycler = (RecyclerView) view.findViewById(R.id.recycler_notificaciones);
            recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            lista = new ArrayList<>();
            adapter = new AdapterNotificaciones(lista, getActivity());

            databaseReference.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    lista.add(dataSnapshot.getValue(Notificacion.class));
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    lista.add(dataSnapshot.getValue(Notificacion.class));
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            recycler.setAdapter(adapter);
        }


        //------------------------------------------------------------------------------------------evento swwipe
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                xMark = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_delete_forever24dp);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) FragmentoNotificaciones.this.getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                AdapterNotificaciones adapter = (AdapterNotificaciones) recycler.getAdapter();
                adapter.remove(swipedPosition);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                if (viewHolder.getAdapterPosition() == -1) {
                    return;
                }

                if (!initiated) {
                    init();
                }

                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth() + 35;
                int intrinsicHeight = xMark.getIntrinsicWidth() + 35;

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public long getAnimationDuration(RecyclerView recyclerView, int animationType, float animateDx, float animateDy) {
                return DEFAULT_SWIPE_ANIMATION_DURATION;
            }
        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleCallback);
        mItemTouchHelper.attachToRecyclerView(recycler);

      /*  String key="";
        Notificacion nt=new Notificacion();
        nt.setImageCode(1);
        for(int i=0; i< 4;i++){
            key=FirebaseUtils.database.getReference(FirebaseUtils.CHILD_NOTIFICACION).child(FirebaseUtils.auth.getCurrentUser().getUid()).push().getKey();
            nt.setKey(key);
            FirebaseUtils.database.getReference(FirebaseUtils.CHILD_NOTIFICACION).child(FirebaseUtils.auth.getCurrentUser().getUid()).child(key).setValue(nt);
        }*/
        return view;
    }

}
