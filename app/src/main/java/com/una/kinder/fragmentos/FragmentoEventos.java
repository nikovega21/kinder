package com.una.kinder.fragmentos;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.actividades.ActividadAgregarEvento;
import com.una.kinder.R;
import com.una.kinder.adapters.AdapterEvents;
import com.una.kinder.entidades.Evento;

import java.util.ArrayList;


public class FragmentoEventos extends Fragment {


    private View view;
    private RecyclerView.ItemDecoration itemDecoration;
    private CalendarView calendario;
    private RecyclerView reciclador;
    ArrayList<Evento> listaEventos;

    FirebaseDatabase database;
    DatabaseReference myRef;

    public FragmentoEventos() {
        // Required empty public constructor
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("eventos");
        view = inflater.inflate(R.layout.fragmento_eventos, container, false);
        listaEventos = new ArrayList<>();
        this.reciclador = (RecyclerView) view.findViewById(R.id.reciclador);
        calendario=(CalendarView) view.findViewById(R.id.calendarioKinder);
        calendario.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Intent intent = new Intent(getActivity(), ActividadAgregarEvento.class);
                intent.putExtra("fecha", dayOfMonth + "/" + (month + 1) + "/" + year);
                startActivityForResult(intent, ActividadAgregarEvento.REQUESTCODE);
            }
        });
//si cambio valor en bd se vuelve a actualizar el valor de la ref
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listaEventos.clear();
                for (DataSnapshot item : dataSnapshot.getChildren()) {
                    Evento evento = item.getValue(Evento.class);
                    listaEventos.add(evento);
                }
                AdapterEvents adapter = new AdapterEvents(listaEventos);
                reciclador.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        reciclador.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true));
        AdapterEvents adapter = new AdapterEvents(listaEventos);
        reciclador.setAdapter(adapter);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ActividadAgregarEvento.REQUESTCODE) {
            if (resultCode == ActividadAgregarEvento.RESULT_OK) {
                String descripcion = data.getStringExtra("descripcion");
                String fecha = data.getStringExtra("fecha");
                String lugar = data.getStringExtra("lugar");
                String hora = data.getStringExtra("hora");
                String grupo = data.getStringExtra("grupo");

                String index = myRef.push().getKey();
                Evento n = new Evento(index, descripcion, fecha, lugar, hora, grupo);
                myRef.child(index).setValue(n);
            } else if (resultCode == ActividadAgregarEvento.RESULT_CANCELED) {

            }

        }
    }


}