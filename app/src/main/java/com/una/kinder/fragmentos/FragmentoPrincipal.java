package com.una.kinder.fragmentos;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.una.kinder.R;
import com.una.kinder.actividades.ActividadCrearPost;
import com.una.kinder.actividades.FirebaseUtils;
import com.una.kinder.adapters.PostAdapter;
import com.una.kinder.entidades.Post;
import com.una.kinder.entidades.Usuario;

import java.util.ArrayList;
import java.util.Map;

public class FragmentoPrincipal extends Fragment {

    private RecyclerView.ItemDecoration itemDecoration;
    private ArrayList<Post> posts;
    private RecyclerView feedView;
    private SwipeRefreshLayout swipeRefreshLayout;
    BottomNavigationView bottomNavigationView;
    private View view;
    private PostAdapter postAdapter;

    //Buttonsss
    private FloatingActionButton crearNuevoPostButton;

    //Firebase
    private FirebaseDatabase database;
    private DatabaseReference databasePosts;

    private Usuario usuarioActual;

    public FragmentoPrincipal() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmento_principal, container, false);
        feedView = view.findViewById(R.id.contenido_feed_inicio);

        ///Cargar aqui la informacion desde la base de datos
        database = FirebaseDatabase.getInstance();
        posts = new ArrayList<>();
        postAdapter = new PostAdapter(getActivity(), posts);

        databasePosts = database.getReference(FirebaseUtils.CHILD_POST);
        databasePosts.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                posts.add(dataSnapshot.getValue(Post.class));
                postAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                postAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                posts.remove(dataSnapshot.getValue(Post.class));
                postAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), LinearLayoutManager.VERTICAL);
        feedView.setLayoutManager(layoutManager);
        feedView.setAdapter(postAdapter);
        feedView.setItemViewCacheSize(20);
        feedView.setDrawingCacheEnabled(true);
        feedView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        feedView.setNestedScrollingEnabled(true);
        // Inicializacion de buttons
        setupButtons(view);

        return view;
    }

    private void setupButtons(View view) {
        swipeRefreshLayout = view.findViewById(R.id.refresh_feed);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                postAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        //Uusuario logueado actual informacion
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseDatabase.getInstance().getReference(FirebaseUtils.CHILD_USUARIO).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    usuarioActual = dataSnapshot.getValue(Usuario.class);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        crearNuevoPostButton = (FloatingActionButton) view.findViewById(R.id.crear_nuevo_post_button);
        crearNuevoPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), ActividadCrearPost.class);
                intent.putExtra("nombre", usuarioActual.getNombre() + " " + usuarioActual.getApellidos());
                intent.putExtra("imagen", usuarioActual.getUrlImagen());
                startActivity(intent);
            }
        });
        bottomNavigationView = (BottomNavigationView) getActivity().findViewById(R.id.navigation);
        feedView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    // Scroll Down
                    if (crearNuevoPostButton.isShown()) {
                        crearNuevoPostButton.hide();
                    }
                } else if (dy < 0) {
                    // Scroll Up
                    if (!crearNuevoPostButton.isShown()) {
                        crearNuevoPostButton.show();
                    }
                }
            }
        });

    }
}
